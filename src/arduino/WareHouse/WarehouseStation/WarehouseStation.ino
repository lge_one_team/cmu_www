/****************************************************************
* File: WarehouseStation
* Project: LG Exec Ed Program Team1
* Copyright: Copyright (c) 2014 Yuntaek Lim
* Versions:
* 0.1 June 2014 - initial version
*
*
*  ---- Description ---- 
*
* This program runs on an Arduino processor with a WIFI shield on station board. 
* This runs in a loop communicating with a HW Ctrl client process in PC. 
*
* Compilation and Execution Instructions: Must be compiled using 
* Arduino IDE VERSION 1.0.4
*
* Parameters: 
*
* Internal Methods: void printConnectionStatus()
*
****************************************************************/
/*
  Pin  Station  Light
  0    1        900 
  1    2        855
  2    3        880
  3    4        885
*/
 
  #include <SPI.h>
  #include <WiFi.h>
 
  #define PORTID 500             // IP socket port#

  #define DEBUG  1

  int i = 0;
  int j = 0;
  int c = 0;
  
  int photocellPin = 0;     // the analog pin the cell is connected
  
  int photocellReading[4];     // the analog reading from the sensor
  boolean bStationState[4] = {false, };
  
  const char REQUEST_BUTTON[6] = "@SSB0";
  const char REQUEST_LIGHT[6] = "@SSL0";

  char cReplyButton[6] = "@SSB0"; 
  char cReplyLight[6] = "@SSL0"; 
  
  char  cInputFromCtrl[5];
  
  const int iPinNumber[4] = {3, 5, 6, 9};
  boolean bButtonState[4] = {1,};
  
  //.. for WIFI  
 char ssid[] = "CMU";           // The network SSID
 int status = WL_IDLE_STATUS;   // Network connection status
 WiFiServer server(PORTID);     // Server connection and port
 char inChar;                   // Character read from client
 IPAddress ip;                  // The IP address of the shield
 IPAddress subnet;              // The IP address of the shield
 long rssi;                     // Wifi shield signal strength
 byte mac[6];                   // Wifi shield MAC address

 void setup() 
 {
   // Initialize a serial terminal for debug messages.
   Serial.begin(9600); 

   for (i = 0; i < 4; i++)
     pinMode(iPinNumber[i], INPUT);      // sets the digital pin 7 as input
  
   // Attempt to connect to Wifi network.
   while ( status != WL_CONNECTED) 
   { 
     Serial.print("[Team One] Attempting to connect to SSID: ");
     Serial.println(ssid);
     status = WiFi.begin(ssid);
   }  
   
   // Print the basic connection and network information.
   printConnectionStatus();
   
   // Start the server and print and message for the user.
   server.begin();
   Serial.println("The Server is started.");
   
 } // setup

 void loop() 
 {
     // Wait for a new client:
     WiFiClient client = server.available();
  
     // When the client connects we send them a couple of messages.
     if (client) 
     {
         client.flush();
         
          char inChar = ' ';   
           
          i = 0;
          j = 0;
          c = 0;   
          while ( inChar != '\n' ) 
          {
             client.write(""); 

            if (client.available())     
              {
  
                // We read a character then we write on to the terminal
                  inChar = client.read();   
                  cInputFromCtrl[c++] = inChar;
//                  Serial.write(inChar);
                  if (c > 4)
                  {
                      break;
                  }
              }else{
                 for (i = 0; i < 4; i++)
                 {
                     photocellReading[i] = analogRead(i);  
                     if (photocellReading[i] < 750)
                     {
                         bStationState[i] = true;
                         cReplyLight[4] = (char) (49+i);
                     }
                     else
                     {
                         bStationState[i] = false;
                     } 
                     
                     bButtonState[i] = digitalRead(iPinNumber[i]);
                      if (bButtonState[i] == false)
                      {
                          cReplyButton[4] = (char) (49+i);
                      }  
                 }
/*            
                  if ( (bStationState[0] | bStationState[1] | bStationState[2] | bStationState[3]) == 0 )
                  {
                          cReplyLight[4] = '0';
                  }
            
                  if ( ( bButtonState[0] & bButtonState[1] & bButtonState[2] & bButtonState[3] ) == 1 )
                  {
                          cReplyButton[4] = '0';
                  }
*/              }
          } //.. while () 

                  if ( (bStationState[0] | bStationState[1] | bStationState[2] | bStationState[3]) == 0 )
                  {
                          cReplyLight[4] = '0';
                  }
            
                  if ( ( bButtonState[0] & bButtonState[1] & bButtonState[2] & bButtonState[3] ) == 1 )
                  {
                          cReplyButton[4] = '0';
                  }
                      
          if (cInputFromCtrl[0] == '@' && cInputFromCtrl[1] == 'S' && cInputFromCtrl[2] == 'S' && cInputFromCtrl[3] == 'B' && cInputFromCtrl[4] == '0')
          {
              client.println(cReplyButton); 
              Serial.print("B");
              Serial.println(cReplyButton[4]);
          }
          else if (cInputFromCtrl[0] == '@' && cInputFromCtrl[1] == 'S' && cInputFromCtrl[2] == 'S' && cInputFromCtrl[3] == 'L' && cInputFromCtrl[4] == '0')
          {
              client.println(cReplyLight); 
              Serial.print("L");
              Serial.println(cReplyLight[4]);
          }
          else
          {
              client.write(""); 
              Serial.println("E");
          }

         client.write(""); 
               
     } // if we are connected
/*     else
     {
                Serial.write(".");
                delay(100);
     }
*/ 
 } // loop
 
/***************************************************************
* The following method prints out the connection information
****************************************************************/

 void printConnectionStatus() 
 {
     // Print the basic connection and network information: 
     // Network, IP, and Subnet mask
     ip = WiFi.localIP();
     Serial.print("Connected to ");
     Serial.print(ssid);
     Serial.print(" IP Address:: ");
     Serial.println(ip);
     subnet = WiFi.subnetMask();
     Serial.print("Netmask: ");
     Serial.println(subnet);
   
     // Print our MAC address.
/*     WiFi.macAddress(mac);
     Serial.print("WiFi Shield MAC address: ");
     Serial.print(mac[5],HEX);
     Serial.print(":");
     Serial.print(mac[4],HEX);
     Serial.print(":");
     Serial.print(mac[3],HEX);
     Serial.print(":");
     Serial.print(mac[2],HEX);
     Serial.print(":");
     Serial.print(mac[1],HEX);
     Serial.print(":");
     Serial.println(mac[0],HEX);
*/

     // Print the wireless signal strength:
     rssi = WiFi.RSSI();
     Serial.print("Signal strength (RSSI): ");
     Serial.print(rssi);
     Serial.println(" dBm");

 } // printConnectionStatus
