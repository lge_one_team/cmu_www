#include <SPI.h>
#include <WiFi.h>
#include <Servo.h>

#define PORTID 500             // IP socket port#
#define BLACK 75
#define WHITE 70
#define OFFSET 14
#define ACCUM_SUM 5

char ssid[] = "CMU";           // The network SSID
int status = WL_IDLE_STATUS;   // Network connection status
WiFiServer server(PORTID);     // Server connection and port
char inChar;                   // Character read from client
IPAddress ip;                  // The IP address of the shield
IPAddress subnet;              // The IP address of the shield
long rssi;                     // Wifi shield signal strength
byte mac[6];                   // Wifi shield MAC address

char Command[] = {0,};
char CommandBuff[] = {0,};
long SensorValue[3] = {0,};
int Servo_Left = 5;
int Servo_Right = 6;
Servo LeftWheel;
Servo RightWheel;
int LeftWheel_Speed = 103;
int RightWheel_Speed = 77;
unsigned char dConer = 0;

unsigned char Robotstop = 0;
unsigned char RobotStatus = 0;
unsigned char GoRobot = 0;
unsigned char previous = 0;

void setup()
{
  Serial.begin(9600);
  Init_WifiServer_Config();
  Init_Servo_Config();
}

void loop()
{
 MWI_Wifi_Receive_Command();  
}

long RCTime(int sensorIn)
{
  long duration = 0;
  pinMode(sensorIn, OUTPUT);         // Sets pin as OUTPUT
  digitalWrite(sensorIn, HIGH);      // Pin HIGH
  pinMode(sensorIn, INPUT);          // Sets pin as INPUT   
  digitalWrite(sensorIn, LOW);       // Pin LOW
  while(digitalRead(sensorIn) != 0){ // Count until the pin goes
    duration++;
  }                                  // LOW (cap discharges)   
  return duration;                   // Returns the duration of the pulse
}

void DDI_ADC_QTISensors(void)
{
  unsigned char Accum = 0;
  unsigned char Pinindex = 0; 
  long TempValue = 0;

  for(Pinindex = 14 ; Pinindex <=16 ; Pinindex++){
    for(Accum = 0 ; Accum <= ACCUM_SUM ; Accum++){
      TempValue += RCTime(Pinindex);
      if(Accum >= ACCUM_SUM){
        SensorValue[(Pinindex-OFFSET)] = (TempValue/Accum);
        TempValue = 0;
   //   PrintSensorValue((Pinindex-OFFSET));
      }
    }
   }
}

void PrintSensorValue(unsigned char SensorIndex)
{
  Serial.print("index  ");
  Serial.print(SensorIndex);
  Serial.print("=");
  Serial.println(SensorValue[SensorIndex]);
}

unsigned char MWI_MakePatternIndex(void)
{
  unsigned char PatternValue[3] = {0,};
  unsigned char index = 0, PatternIndex = 0;

  for(index = 0; index <3 ; index++){    
    if(SensorValue[index] >= BLACK){
      PatternValue[index] = 1;
    }
    else if(SensorValue[index] <= WHITE){
      PatternValue[index] = 0;
    }
    else ;
  }  
  PatternIndex = 4*PatternValue[0]+2*PatternValue[1]+PatternValue[2];
  return PatternIndex;
}

void MWI_Make_Pattern_Recog()
{
  unsigned char Pattern = MWI_MakePatternIndex();
  unsigned char rValue = 2;
  
  switch(Pattern){
    case 0:
      if(previous == 2 || previous == 4){
        DDI_ServoMotor_SetTurnLeft();
      }else if(previous != 0){
        DDI_ServoMotor_SetTurnRight();
      }
      break;
    case 1:
      DDI_ServoMotor_SetTurnRight();
      break;
    case 2:
      dConer =0;
      Robotstop = 0;
      DDI_ServoMotor_SetGoAhead_Full();
      break;
    case 3:
      DDI_ServoMotor_SetTurnLeft();
      break;
    case 4:
      if(Robotstop ==1 && dConer ==0){
        GoRobot = 0;
        rValue = 0;
        dConer = 1;
        DDI_ServoMotor_SetStop();
      }else{
        if(dConer == 0){
            DDI_ServoMotor_SetTurnLeft();
        }else{
            DDI_ServoMotor_SetTurnRight();
        }
      }
      break;
    case 5:
       rValue = 0;
      if(previous == 4){
        Robotstop = 0;
      }else Robotstop = 1;
      DDI_ServoMotor_SetGoAhead();
      break;
    case 6:
      DDI_ServoMotor_SetTurnRight();
      break;
    case 7:
      DDI_ServoMotor_SetTurnRight();
      break;
    default:
      DDI_ServoMotor_SetStop();
      rValue = 1;
      break;      
    }
    
  if(previous != Pattern){
      previous = Pattern;
      Serial.print("PatternIndex ");
      Serial.print("= ");
     Serial.println(Pattern);  
  }
  if(rValue == 0){
    RobotStatus = 0;   // Arrived station
  }else if(rValue == 1){
    RobotStatus = 1;  // Lost Line
  }else{
    RobotStatus = 2;  // busy
  }  
}

void DDI_ServoMotor_SetGoAhead_Full(void){
  LeftWheel.write(114);
  RightWheel.write(63);
}

void DDI_ServoMotor_SetGoAhead(void){
  LeftWheel.write(LeftWheel_Speed); //서보모터 각도 지정,
  RightWheel.write(RightWheel_Speed);
}

void DDI_ServoMotor_SetTurnRight(void){
    LeftWheel.write(LeftWheel_Speed); //서보모터 각도 지정,
    RightWheel.write(LeftWheel_Speed);    
}

void DDI_ServoMotor_SetTurnLeft(void){
  LeftWheel.write(RightWheel_Speed); //서보모터 각도 지정,
  RightWheel.write(RightWheel_Speed);
}

void DDI_ServoMotor_SetStop(void){
  LeftWheel.write(90); //서보모터 각도 지정,
  RightWheel.write(90);
}

void Init_Servo_Config(void)
{
  LeftWheel.attach(Servo_Left); //myservo가 연결된 핀 지정, 모터제어 시작
  RightWheel.attach(Servo_Right); //myservo가 연결된 핀 지정, 모터제어 시작
}

void Init_WifiServer_Config(void)
{
  // Attempt to connect to Wifi network.
  while (status != WL_CONNECTED) 
  { 
    Serial.print("Attempting to connect to SSID: ");
    Serial.println(ssid);
    status = WiFi.begin(ssid);
  }     
  // Print the basic connection and network information.
  printConnectionStatus();   
  // Start the server and print and message for the user.
  server.begin();
  Serial.println("The Server is started.");  
}
/***************************************************************
 * The following method prints out the connection information
 ****************************************************************/
void printConnectionStatus() 
{
  // Print the basic connection and network information: 
  // Network, IP, and Subnet mask
  ip = WiFi.localIP();
  Serial.print("Connected to ");
  Serial.print(ssid);
  Serial.print(" IP Address:: ");
  Serial.println(ip);
  subnet = WiFi.subnetMask();
  Serial.print("Netmask: ");
  Serial.println(subnet);

  // Print our MAC address.
  WiFi.macAddress(mac);
  Serial.print("WiFi Shield MAC address: ");
  Serial.print(mac[5],HEX);
  Serial.print(":");
  Serial.print(mac[4],HEX);
  Serial.print(":");
  Serial.print(mac[3],HEX);
  Serial.print(":");
  Serial.print(mac[2],HEX);
  Serial.print(":");
  Serial.print(mac[1],HEX);
  Serial.print(":");
  Serial.println(mac[0],HEX);   
  // Print the wireless signal strength:
  rssi = WiFi.RSSI();
  Serial.print("Signal strength (RSSI): ");
  Serial.print(rssi);
  Serial.println(" dBm");
} // printConnectionStatus

void MWI_Wifi_Receive_Command(void)
{
  unsigned int OrderIndex = 0,ByteIndex=0,Temp = 0;  
  // Wait for a new client:
  WiFiClient client = server.available();
  // When the client connects we send them a couple of messages.
  if (client){ 
    // Clear the input buffer.
    client.flush(); 
    // We first write a debug message to the terminal then send
    // the data to the client.
    Serial.println("Sending data to client...");     
    // Now we switch to read mode...
    Serial.println("Reading data from client...");  
    char inChar = ' ';  
    OrderIndex = 0;
    // Checks to see if data is available from the client 
      while (inChar != '\n'){ 
        // We read a character then we write on to the terminal
          if (client.available()){ 
              inChar = client.read();
              CommandBuff[ OrderIndex++] = inChar;
             // Serial.println(inChar);
              if(OrderIndex >4){
                break;
              }                
          }else{       
          if(GoRobot == 1){
              DDI_ADC_QTISensors();
              MWI_Make_Pattern_Recog();
            } 
          }     
      }
      
    for(Temp = 0 ; Temp < OrderIndex ;Temp++ ){
      if(Command[Temp] != CommandBuff[Temp]){
        Command[Temp]= CommandBuff[Temp];
      }      
    }
    
    Serial.println("//////////////////////");
    Serial.println(Command); 
     
    if(Command[0] != '@'){
      RobotStatus = 9 ;// protocol error
    }else{
        if((Command[1] == 'C') && (Command[2] == 'R') && (Command[3] == 'G')){
            GoRobot = 1;
            Robotstop = 0;
            client.println("@CRG0");
         }   
      
        if((Command[1] == 'S') && (Command[2] == 'R') && (Command[3] == 'E')){
            if(RobotStatus == 0){  // Arrived station
              client.println("@SRE0");
            }else if(RobotStatus == 1){  // Lost pattern
              client.println("@SRE1");
            }else if(RobotStatus == 2){  // busy, moving to next station
              client.println("@SRE2");
            }else if(RobotStatus == 9){
              client.println("@SRE9");;
            }
        }   
    }
  } // if we are connected
}


