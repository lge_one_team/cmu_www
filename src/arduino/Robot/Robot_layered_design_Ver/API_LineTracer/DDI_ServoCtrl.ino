#include <Servo.h>

int Servo_Left = 5;
int Servo_Right = 6;
Servo LeftWheel;
Servo RightWheel;

void DDI_ServoCtrl_SetInitConfig(void)
{
  LeftWheel.attach(Servo_Left); //myservo가 연결된 핀 지정, 모터제어 시작
  RightWheel.attach(Servo_Right); //myservo가 연결된 핀 지정, 모터제어 시작
}

void DDI_ServoCtrl_WheelControl(unsigned int LeftWheelSpeed,unsigned int RightWheelSpeed)
{
  LeftWheel.write(LeftWheelSpeed);
  RightWheel.write(RightWheelSpeed);
}
