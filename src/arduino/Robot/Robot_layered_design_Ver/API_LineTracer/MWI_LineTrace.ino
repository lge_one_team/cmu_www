#include <SPI.h>

#define BLACK 75
#define WHITE 70
#define OFFSET 14
#define ACCUM_SUM 5

#define GO_FULL_L  117
#define GO_FULL_R  62
#define GO_HALF_L  105
#define GO_HALF_R  77
#define TURN_LEFT  77
#define TURN_RIGHT 105
#define STOP_WHEEL 90

long SensorValue[3] = {0,};
unsigned int LeftWheel_Speed = STOP_WHEEL;
unsigned int RightWheel_Speed = STOP_WHEEL;
unsigned char PatternCnt[8] = {0,};
unsigned char previous = 10;
unsigned char Pattern = 0;
unsigned char dConer = 0;

void MWI_LineTrace_RecognizeLine(void)
{
  unsigned char Accum = 0;
  unsigned char index = 0; 
  long TempValue = 0;

  for(index = 14 ; index <=16 ; index++){
    for(Accum = 0 ; Accum <=ACCUM_SUM ; Accum ++){
      TempValue += DDI_QTISensorCtrl_RCTime(index);
      if(Accum>=ACCUM_SUM){
        SensorValue[(index-OFFSET)] = (TempValue/Accum);
        TempValue = 0;
        #ifdef __DEBUG__  
        Debug_LineTrace_PrintSensor((index-OFFSET));
        #endif
      }
    }
  }
}

void MWI_LineTrace_TracingLine()
{
  Pattern = LineTrace_MakePattern();
  RobotStatus = LineTrace_DirectionCtl(Pattern); 
  DDI_ServoCtrl_WheelControl(LeftWheel_Speed,RightWheel_Speed);
}

static unsigned char LineTrace_MakePattern(void)
{
  unsigned char PatternValue[3] = {0,};
  unsigned char index = 0, PatternIndex = 0;

  for(index = 0; index <3 ; index++){    
    if(SensorValue[index] >= BLACK){
      PatternValue[index] = 1;
    }else if(SensorValue[index] <= WHITE){
      PatternValue[index] = 0;
    }else{
      ;
    }
  } 
  PatternIndex = 4*PatternValue[0]+2*PatternValue[1]+PatternValue[2];
#ifdef __DEBUG__  
  Debug_LineTrace_Print_PatternIndex(PatternIndex);
#endif  
  return PatternIndex;
}

static unsigned char LineTrace_DirectionCtl(unsigned char InPattern){
  unsigned char rValue = 2;
  
    switch(InPattern){
    case 0:
      if(previous == 2 || previous == 4){
        LineTrace_SetTurnLeft();
      }else if(previous != 0){
        LineTrace_SetTurnRight();
      }else;
      break;
    case 1:
      LineTrace_SetTurnRight();
      break;
    case 2:
      dConer =0;
      Robotstop = 0;
      LineTrace_SetGoAhead_Full();
      break;
    case 3:
     if(previous ==1){
      LineTrace_SetGoAhead();
     }else if(previous ==7){
      LineTrace_SetGoAhead();
     }else{
      LineTrace_SetTurnRight();
     }
      break;
    case 4:
      if(Robotstop ==1 && dConer == 0){
        GoRobot = 0;
        rValue = 0;
        dConer = 1;
        LineTrace_SetStop();
      }else{
        if(dConer == 0){
            LineTrace_SetTurnLeft();
        }else{
            LineTrace_SetTurnRight();
        }
      }
      break;
    case 5:
       rValue = 0;
      if(previous == 4){
        Robotstop = 0;
      }else {
        Robotstop = 1;
      }
      LineTrace_SetGoAhead();
      break;
    case 6:
      LineTrace_SetTurnRight();
      break;
    case 7:
      LineTrace_SetTurnRight();
      break;
    default:
      LineTrace_SetStop();
      rValue = 1;
      break;      
    }
    
   if(previous != InPattern){
      previous = InPattern;
      #ifdef __DEBUG__
      Serial.print("***********InPatternIndex********** ");
      Serial.print("= ");
      Serial.println(InPattern);
      #endif
  }
  return rValue;
}

static void LineTrace_SetGoAhead_Full(void)
{
  LeftWheel_Speed = GO_FULL_L;
  RightWheel_Speed = GO_FULL_R;
  Serial.println("GO_AHEAD_FULL_SPEED!!! GO_AHEAD_FULL_SPEED!!!");
}

static void LineTrace_SetGoAhead(void)
{
  LeftWheel_Speed = GO_HALF_L;
  RightWheel_Speed = GO_HALF_R;
  Serial.println("GO_AHEAD_HALF_SPEED!!! GO_AHEAD_HALF_SPEED!!! ");
}

static void LineTrace_SetStop(void)
{
  LeftWheel_Speed = STOP_WHEEL;
  RightWheel_Speed = STOP_WHEEL;
  Serial.println("STOP!!! STOP!!! STOP!!! STOP!!! STOP!!! STOP!!!");
}

static void LineTrace_SetTurnRight(void)
{
  LeftWheel_Speed = TURN_RIGHT;
  RightWheel_Speed = TURN_RIGHT;
  Serial.println("TURN_RIGHT_HALF_SPEED!! TURN_RIGHT_HALF_SPEED!! ");
}

static void LineTrace_SetTurnLeft(void)
{
  LeftWheel_Speed = TURN_LEFT;
  RightWheel_Speed = TURN_LEFT;
  Serial.println("TURN_LEFT_HALF_SPEED!!! TURN_LEFT_HALF_SPEED!!! ");
}

static void Debug_LineTrace_PrintSensor(unsigned char SensorIndex)
{
  Serial.print("Sensor_Index  ");
  Serial.print(SensorIndex);
  Serial.print(" = ");
  Serial.println(SensorValue[SensorIndex]);
}

static void Debug_LineTrace_Print_PatternIndex(unsigned char Index)
{
  Serial.print("PatternIndex");
  Serial.print(" = ");
  Serial.println(Index);  
}
