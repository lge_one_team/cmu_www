#include <SPI.h>
#include <WiFi.h>
char Command[] = {0,};
char CommandBuff[] = {0,};
char inChar;                   // Character read from client

void MWI_Command_ReceiveComm(void)
{
  unsigned int OrderIndex = 0,ByteIndex=0,Temp = 0; 
  WiFiClient client = server.available();
  if (client){
    client.flush();
    Serial.println("Sending data to client...");     
    Serial.println("Reading data from client...");  
    char inChar = ' ';  
    OrderIndex = 0;
    while (inChar != '\n'){
      if(client.available()){
        inChar = client.read();
        CommandBuff[OrderIndex++] = inChar;
        if(OrderIndex >4){
          break;
        }                
      }else{
        if(GoRobot == 1){
          MWI_LineTrace_RecognizeLine();
          MWI_LineTrace_TracingLine();          
        }
      }
    }
    
    for(Temp = 0 ; Temp <OrderIndex ;Temp++ ){
      if(Command[Temp] != CommandBuff[Temp]){
        Command[Temp]= CommandBuff[Temp];
      }      
    }
    
    Serial.println("=============== Command ===============");
    Serial.println(Command); 

    if(Command[0] != '@'){
      RobotStatus = 9;
    }else{
        if((Command[1] == 'C') && (Command[2] == 'R') && (Command[3] == 'G')){
            GoRobot = 1;
            Robotstop = 0;
            client.println("@CRG0");
         }   
      
        if((Command[1] == 'S') && (Command[2] == 'R') && (Command[3] == 'E')){
            if(RobotStatus == 0){        // Arrived station
              client.println("@SRE0");
            }else if(RobotStatus == 1){  // Lost pattern
              client.println("@SRE1");
            }else if(RobotStatus == 2){  // busy, moving to next station
              client.println("@SRE2");
            }else if(RobotStatus == 9){  // Protocol Error
              client.println("@SRE9");;
            }
        }   
    }
  }
}
