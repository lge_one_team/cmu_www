long DDI_QTISensorCtrl_RCTime(int sensorIn)
{
  long duration = 0;
  pinMode(sensorIn, OUTPUT);         // Sets pin as OUTPUT
  digitalWrite(sensorIn, HIGH);      // Pin HIGH
  pinMode(sensorIn, INPUT);          // Sets pin as INPUT
  digitalWrite(sensorIn, LOW);       // Pin LOW
  while(digitalRead(sensorIn) != 0){ // Count until the pin goes
    duration++;
  }                                  // LOW (cap discharges)   
  return duration;                   // Returns the duration of the pulse
}
