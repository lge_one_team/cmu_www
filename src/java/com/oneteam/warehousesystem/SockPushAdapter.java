package com.oneteam.warehousesystem;

public class SockPushAdapter {
	SockServerSupervisor srvSock;
	
	public SockPushAdapter () {
		srvSock = null;
	}
	
	public void setSock (SockServerSupervisor srvSock) {
		this.srvSock = srvSock;
	}
	
	public void updateStatus (String strWhat) {
		if (this.srvSock != null) {
			this.srvSock.updateSomething (strWhat);
		}
	}
}
