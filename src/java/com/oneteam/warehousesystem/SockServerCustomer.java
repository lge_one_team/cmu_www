package com.oneteam.warehousesystem;

import java.util.ArrayList;
import java.util.StringTokenizer;

public class SockServerCustomer extends SockServer {
	WarehouseManager whMngr;
	
	public SockServerCustomer (WarehouseManager whMngr) {
		super (1111, 1);
		this.whMngr = whMngr;
	}

	@Override
	public void rcvpkt (String rcvpkt) {
		StringTokenizer st = new StringTokenizer (rcvpkt, ",");
		String          s  = st.nextToken ();

		System.out.println ("SockServerCustomer get a packet : " + rcvpkt);
        if (s.equals ("CW1")) {
			DBManager dbmngr  = DBManager.getInstance ();
			String    dbwidget= dbmngr.getAllWidgetInfo ();
			
			if (dbwidget != null) {
				mkandsndpkt ("WC1", dbwidget);
			}
        } else if (s.equals ("CW2")) {
			ArrayList<Widget> widgets = new ArrayList<Widget> ();
			int               counter = 0;

			while (st.hasMoreTokens ()) {
				Widget wg = new Widget ();
				String wn = st.nextToken ();

				if (wn.equals ("E")) {
					//
					break;
				} else {
					wg.name     = wn;
					wg.quantity = Integer.parseInt(st.nextToken ());
					widgets.add (wg);
					counter++;
				}
            }
			if (counter > 0) {
				whMngr.placeOrder (widgets);
			}
			mkandsndpkt ("WC2", null);
		} else {
			System.out.println("but it is unKnown protocol ");
		}
	}
}
