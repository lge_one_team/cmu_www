package com.oneteam.warehousesystem;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class SendEmailThread extends Thread {

	private String send_to = "lgoneteamcustomer@gmail.com";
	private String subj = "[OneTeam] Your order is shipped!!";
	private String msg = "Order is shipped ";
		
	public SendEmailThread (){
	}
	
	public SendEmailThread ( String msg ){
		this.msg= msg;
	}
	
	public SendEmailThread ( String subj, String msg ){
		this.subj = subj;
		this.msg= msg;
	}
	
	public SendEmailThread ( String send_to, String subj, String msg ){
		this.send_to = send_to;
		this.subj = subj;
		this.msg= msg;
	}
	
	@Override
	public void run() {
		
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");
		final String username = "lgoneteamsupervisor@gmail.com";
		final String password = "dkzlxprcu";
 
		Session session = Session.getInstance(props,
		  new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		  });		
		
		try {
			 
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("lgoneteamsupervisor@gmail.com"));
			message.setRecipients(Message.RecipientType.TO,
				InternetAddress.parse("" + send_to ));
			//message.setSubject("Testing Subject on my laptop");
			message.setSubject("" + subj);
			message.setText("" + msg 
				+ "\n\n Thanks for using OneTeam Willy Widget's Company!");
 
			Transport.send(message);
 
			System.out.println("Done");
 
		} catch (MessagingException e) {
			System.out.println("MessagingException occured during send email");
			e.printStackTrace();			
		} catch (Exception e) {
			System.out.println("Exception occured during send email");
			e.printStackTrace();			
		}		
		
	} // END run()	
	
}
