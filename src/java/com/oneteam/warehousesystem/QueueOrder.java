package com.oneteam.warehousesystem;

import java.util.ArrayList;

public class QueueOrder {
	public ArrayList<Widget> widgets;
	public int               orderdb_id;
	public QueueOrder (ArrayList<Widget> widgets, int orderdb_id) {
		this.widgets    = widgets;
		this.orderdb_id = orderdb_id;
	}
}
