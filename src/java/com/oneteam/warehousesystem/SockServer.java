package com.oneteam.warehousesystem;

import java.net.*;
import java.io.*;

public abstract class SockServer {
	ServerThread srvth;
	
	public SockServer () {
		this (2121, 1);
	}
	public SockServer (int port, int maxClients) {
		srvth = new ServerThread (this, port);
		srvth.start ();
		// maxClients must be supported?
	}
	public void sendpkt (String sndpkt) {
		srvth.sendpkt(sndpkt);
	}
	public abstract void rcvpkt (String rcvpkt);
	
	protected void mkandsndpkt (String head, String body) {
		StringBuilder sb = new StringBuilder ();
		
		sb.append (head);
		sb.append (",");
		if (body != null) {
			sb.append (body);
			sb.append (",");
		}
		sb.append ("E");
		System.out.println ("[SENDPKT]" + sb.toString());
		sendpkt (sb.toString());
	}
}

class ServerThread extends Thread {
	SockServer         caller;
	
	ServerSocket       srvsoc;
	Socket             soc;
	
	ObjectOutputStream oos;
	ObjectInputStream  ois;

	public ServerThread () {
		this (null, 2121);
	}
	
	public ServerThread (SockServer caller, int port) {
		try {
			srvsoc = new ServerSocket (port);
			System.out.println ("open a server with port number : " + port);
		} catch (IOException e) {
			System.out.println (e.getMessage ());
		}
		this.caller = caller;
		this.oos    = null;
		this.ois    = null;
	}

	public void run () {
		while (true) {
			try {			
				soc = srvsoc.accept ();
				oos = new ObjectOutputStream (soc.getOutputStream ());
				ois = new ObjectInputStream (soc.getInputStream ());
				System.out.println ("connected with " + soc.getInetAddress () + " ...ready to communicate");
				String rcvpkt = null;
				while (true) {
					try {
						if ((rcvpkt = (String)ois.readObject()) != null) {
							caller.rcvpkt (rcvpkt);
						}
					} catch (ClassNotFoundException e) {
						// 
					}
				}
			} catch (IOException e) {
				System.out.println (e.getMessage ());
			} finally {
				try {
					if (oos != null) {
						oos.close ();
					}
				} catch (IOException e) {
					System.out.println (e.getMessage ());
				}
				oos = null;
			}
		}
	}
	
	public void sendpkt (String sndpkt) {
		if (this.oos == null) {
			return;
		}
		try {
			oos.writeObject (sndpkt);
			oos.flush ();
		} catch (IOException e) {
			// 
		}
	}
}