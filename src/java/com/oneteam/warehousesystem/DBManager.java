package com.oneteam.warehousesystem;

import java.util.ArrayList;

import com.oneteam.warehousesystem.db.DBAdaptor;
import com.oneteam.warehousesystem.db.DBConnBase;
import com.oneteam.warehousesystem.db.DBConnector;
import com.oneteam.warehousesystem.db.Order;
import com.oneteam.warehousesystem.db.Station;
import com.oneteam.warehousesystem.db.WidgetInfo;

public class DBManager {
	
	private static final int MAX_ROBOT = 1;
	
	private static DBManager dbm = new DBManager();
	private static DBConnBase dbc = new DBAdaptor();

	public static DBManager getInstance() {
		return dbm;
	}

	public synchronized int updateWidgetQuantityOnStation(int station_id, int widget_id, int setcount ){
		return dbc.updateWidgetQuantityOnStation(station_id, widget_id, setcount );
	}
	public synchronized int incWidget(int station_id, int widget_id, int inc_count ){
		return dbc.incWidget(station_id, widget_id, inc_count);
	}
	public synchronized int incWidget(int station_id, String widget_name, int inc_count ){
		return dbc.incWidget(station_id, WidgetInfo.getIdByName(widget_name), inc_count);
	}
	public synchronized int decWidget(int station_id, int widget_id, int dec_count ){
		return dbc.decWidget(station_id, widget_id, dec_count);
	}
	public synchronized int decWidget(int station_id, String widget_name, int dec_count ){
		return dbc.decWidget(station_id, WidgetInfo.getIdByName(widget_name), dec_count);
	}
	public synchronized Station getStationFromDB(int station_id ){
		return dbc.getStationFromDB(station_id);
	}
	public synchronized ArrayList<Station> getAllStationInfo(){
		return dbc.getAllStationInfo();
	}
	public boolean isDBExist(String findingDB){
		return dbc.isDBExist(findingDB);
	}
	
	
	/* Order related method*/
	public synchronized int insertNewOrder(int set_Status, String content ){
		return dbc.insertNewOrder(set_Status, content);
	}
	public synchronized int updateOrderStatus(int order_id, int set_Status ){
		return dbc.updateOrderStatus(order_id, set_Status);
	}	
	public synchronized int getOrderStatus(int order_id){
		Order od = dbc.getOrderFromDB(order_id);
		return od.getStatus();
	}
	public synchronized ArrayList<Order> getAllOrders() {
		return dbc.getAllOrders();
	}
	public synchronized String getAllOrderItem() {
		 ArrayList<Order> aod = dbc.getAllOrders();
		 StringBuilder    sb  = new StringBuilder("");
		 for (Order od : aod) {
			 sb.append(od.getOrder_id ()).append(",").append(Order.order_hashmap.get(od.getStatus())).append(",");
			 sb.append(od.getContent()).append(",");
		 }
		 if (sb.length() > 0) {
			 sb.deleteCharAt (sb.length() - 1);
		 } else {
			 sb.append("none");
		 }
		 System.out.println("[getAllOrderItem] return:" + sb.toString());
		 return sb.toString();
	}
	public synchronized Order getOrderFromDB(int order_id ){
		return dbc.getOrderFromDB(order_id);
	}
	public synchronized String getOrderItem (int order_id) {
		Order         od = dbc.getOrderFromDB(order_id);
		StringBuilder sb = new StringBuilder("");
		
		sb.append(od.getOrder_id ()).append(",").append(Order.order_hashmap.get(od.getStatus())).append(",");
		sb.append(od.getContent());
		System.out.println("[getOrderItem] return:" + sb.toString());
		return sb.toString();
	}
	 public synchronized void deleteStationsTable(){
		 dbc.deleteStationsTable();
	 }
	 public synchronized void deleteOrdersTable(){
		 dbc.deleteOrdersTable();
	 }
	 public synchronized void resetStationsTable(){
		 dbc.resetStationsTable();
	 }
	 public synchronized void resetOrdersTable(){
		 dbc.resetOrdersTable();
	 }
	 public synchronized void resetAllTables(){
		 dbc.resetAllTables();
	 }
	 public synchronized void deleteDB(){
		 dbc.deleteDB();
	 }
	
	
	public synchronized String getAllWidgetInfo() {
		int total_count_widget1 = 0;
		int total_count_widget2 = 0;
		int total_count_widget3 = 0;
		int total_count_widget4 = 0;
		int total_count_widget5 = 0;
		
		ArrayList <Station> sts = dbc.getAllStationInfo();
		for ( Station st : sts ) {
			total_count_widget1 += st.item1_cnt;
			total_count_widget2 += st.item2_cnt;
			total_count_widget3 += st.item3_cnt;
			total_count_widget4 += st.item4_cnt;
			total_count_widget5 += st.item5_cnt;
		}
		
		StringBuilder sb = new StringBuilder("");
		sb.append(WidgetInfo.getNameById(1)).append(",").append(total_count_widget1).append(",");
		sb.append(WidgetInfo.getNameById(2)).append(",").append(total_count_widget2).append(",");
		sb.append(WidgetInfo.getNameById(3)).append(",").append(total_count_widget3).append(",");
		sb.append(WidgetInfo.getNameById(4)).append(",").append(total_count_widget4).append(",");
		sb.append(WidgetInfo.getNameById(5)).append(",").append(total_count_widget5);
		System.out.println("[getAllWidgetInfo] return:" + sb.toString());
		return sb.toString();
	}
	
	public synchronized String getAllWidgetInfo( boolean onlyname ) {
		int total_count_widget1 = 0;
		int total_count_widget2 = 0;
		int total_count_widget3 = 0;
		int total_count_widget4 = 0;
		int total_count_widget5 = 0;
		
		ArrayList <Station> sts = dbc.getAllStationInfo();
		for ( Station st : sts ) {
			total_count_widget1 += st.item1_cnt;
			total_count_widget2 += st.item2_cnt;
			total_count_widget3 += st.item3_cnt;
			total_count_widget4 += st.item4_cnt;
			total_count_widget5 += st.item5_cnt;
		}
		
		StringBuilder sb = new StringBuilder("");
		if ( onlyname == false) {
			sb.append(WidgetInfo.getNameById(1)).append(",").append(total_count_widget1).append(",");
			sb.append(WidgetInfo.getNameById(2)).append(",").append(total_count_widget2).append(",");
			sb.append(WidgetInfo.getNameById(3)).append(",").append(total_count_widget3).append(",");
			sb.append(WidgetInfo.getNameById(4)).append(",").append(total_count_widget4).append(",");
			sb.append(WidgetInfo.getNameById(5)).append(",").append(total_count_widget5);
			System.out.println("[getAllWidgetInfo] return:" + sb.toString());
		}
		else {
			sb.append(WidgetInfo.getNameById(1)).append(",");
			sb.append(WidgetInfo.getNameById(2)).append(",");
			sb.append(WidgetInfo.getNameById(3)).append(",");
			sb.append(WidgetInfo.getNameById(4)).append(",");
			sb.append(WidgetInfo.getNameById(5)).append(",");
			System.out.println("[getAllWidgetInfo] return:" + sb.toString());
		}

		return sb.toString();
	}
	
	public synchronized int getMaxRotbotLoad () {
		return MAX_ROBOT;
	}
	
	public synchronized int getItemsAtStation (int station_id, String widget_name) {
		Station st = getStationFromDB(station_id);
		int result = st.getWidgetCount(WidgetInfo.getIdByName(widget_name));
		return result;
	}
	
	public synchronized String getAllItems () {
		StringBuilder sb        = new StringBuilder ("");
		ArrayList <Station> sts = dbc.getAllStationInfo ();
		int          station_id = 1;
		
		for ( Station st : sts ) {
			sb.append("[ST],").append(station_id).append(",");
			sb.append(WidgetInfo.getNameById(1)).append(",").append(st.item1_cnt).append(",");
			sb.append(WidgetInfo.getNameById(2)).append(",").append(st.item2_cnt).append(",");
			sb.append(WidgetInfo.getNameById(3)).append(",").append(st.item3_cnt).append(",");
			sb.append(WidgetInfo.getNameById(4)).append(",").append(st.item4_cnt).append(",");
			sb.append(WidgetInfo.getNameById(5)).append(",").append(st.item5_cnt).append(",");
			station_id++;
		}
		sb.deleteCharAt (sb.length() - 1);
		System.out.println ("[getAllItems] return:" + sb.toString());
		return sb.toString ();
	}
}
