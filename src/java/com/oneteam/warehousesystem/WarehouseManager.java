package com.oneteam.warehousesystem;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import com.oneteam.facilitymanager.FacilityManager;
import com.oneteam.facilitymanager.OrderWidget;

public class WarehouseManager extends Thread implements Observer {
	int                   curorderdbid;
	ArrayList<QueueOrder> quOrders = new ArrayList<QueueOrder>();
	FacilityManager       faMngr;
	SockPushAdapter       sPushApt;
	String                facilitystatus = new String ("Idle");
	QueueOrder            newOrder;
		
	public WarehouseManager (SockPushAdapter sPushApt) {
		this.sPushApt     = sPushApt;
		this.curorderdbid = 0;
		this.newOrder     = null;
			
		this.start ();
		this.faMngr       = FacilityManager.getInstance ();
		this.faMngr.addObserver (this);
	}

	public void placeOrder (ArrayList<Widget> widgets) {
		this.newOrder = new QueueOrder (widgets, insertDataBase (widgets));
	}

	public String getStatusFacility () {
		return this.facilitystatus;
	}
	
	@Override
	public void update (Observable arg0, Object arg1) {
		if (arg0 instanceof FacilityManager) {
			String temp = this.faMngr.getFacilityStatus ();
			
			if (temp != null) {
				facilitystatus = temp;
				this.sPushApt.updateStatus ("FacilityStatusChanged");
			} else if (faMngr.isOrderCompleted () == true) {
				if (this.curorderdbid > 0) {
					DBManager dbmngr = DBManager.getInstance ();
				
					dbmngr.updateOrderStatus (this.curorderdbid, 3);
					sendEmailToCustomer (dbmngr.getOrderItem(this.curorderdbid));
					sPushApt.updateStatus ("OrderChanged," + this.curorderdbid);
					this.curorderdbid = 0;
				}
			}
		}
	}

	public void run () {
		while (true) {
			if (this.curorderdbid == 0) {
				for (QueueOrder qo:quOrders) {
					if (sendOrderToNext (qo) == true) {
						this.curorderdbid = qo.orderdb_id;
						quOrders.remove (qo);
						break;
					}
				}
			}
			try {
				sleep (500);
			} catch (InterruptedException e) {
				System.out.println (e.getMessage ());
			}
			if (this.newOrder != null) {
				quOrders.add (this.newOrder);
				this.newOrder = null;
			}
		}
	}
	
	private int insertDataBase (ArrayList<Widget> widgets) {
		DBManager     dbmngr = DBManager.getInstance ();
		StringBuilder sb     = new StringBuilder ("");
		int           orderdb_id;

		orderdb_id = 0;
		for (Widget w:widgets) {
			int  quantity;

			quantity = w.quantity;
			if (quantity > 0) {
				sb.append (w.name);
				sb.append (" ");
				sb.append (quantity);
				sb.append (" ");
			}
		}
		orderdb_id = dbmngr.insertNewOrder (0, sb.toString ());
		sPushApt.updateStatus ("OrderChanged," + orderdb_id);
		return orderdb_id;
	}
	
	private boolean sendOrderToNext (QueueOrder qo) {
		DBManager              dbmngr      = DBManager.getInstance ();
		int                    maxstations = 3;
		ArrayList<OrderWidget> odWg        = new ArrayList<OrderWidget>();
		
		for (Widget w:qo.widgets) {
			int  quantity;

			quantity = w.quantity;
			for (int i = 1; i <= maxstations; i++) {
				int cnt;

				if (quantity == 0) {
					break;
				}
				cnt = dbmngr.getItemsAtStation (i, w.name);
				System.out.println (w.name + ": " + quantity + ", DB has " + cnt);
				if (cnt > 0) {
					OrderWidget ow = new OrderWidget ();
					
					ow.setStationId (i);
					ow.setWidgetName (w.name);
					if (cnt > quantity) {
						ow.setNumOfWidget (quantity);
						quantity  = 0;
					} else {
						ow.setNumOfWidget (cnt);
						quantity -= cnt;
					}
					odWg.add (ow);
				}
			}
			if (quantity > 0) {
				if (dbmngr.getOrderStatus(qo.orderdb_id) != 2) {
					dbmngr.updateOrderStatus (qo.orderdb_id, 2);
					sPushApt.updateStatus ("OrderChanged," + qo.orderdb_id);
				}
				return false;
			}
		}
		for (OrderWidget ow : odWg) {
			dbmngr.decWidget (ow.getStationId (), ow.getWidgetName(), ow.getNumOfWidget());
		}
		faMngr.sendAnOrder (odWg);
		dbmngr.updateOrderStatus (qo.orderdb_id, 1);
		sPushApt.updateStatus ("OrderChanged," + qo.orderdb_id);
		sPushApt.updateStatus ("DBChanged");
		return true;
	}
	
	private void sendEmailToCustomer (String strTitle) {
		Thread sendGmai3 = new SendEmailThread (strTitle, "we shipped your order");
		sendGmai3.start();
	}
}
