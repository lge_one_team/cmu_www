package com.oneteam.warehousesystem.db;

import java.util.HashMap;

public class WidgetInfo {

		public static final int WIDGET_ID_1 = 1;
		public static final int WIDGET_ID_2 = 2;
		public static final int WIDGET_ID_3 = 3;
		public static final int WIDGET_ID_4 = 4;
		public static final int WIDGET_ID_5 = 5;
		
		public static final String WIDGET_BASEBALL = "BaseBall";
		public static final String WIDGET_SOCKERBALL = "SoccerBall";
		public static final String WIDGET_BASKETBALL = "BasketBall";
		public static final String WIDGET_FOOTBALL = "FootBall";
		public static final String WIDGET_SOFTBALL = "SoftBall";
		
		public static HashMap<Integer, String> wds = new HashMap<Integer, String>();
		
		private static WidgetInfo wi = new WidgetInfo();
		
		private WidgetInfo () {
			init();
		}

		private void init() {
			// TODO Auto-generated method stub
			wds.put(WIDGET_ID_1 , WIDGET_BASEBALL);
			wds.put(WIDGET_ID_2 , WIDGET_SOCKERBALL);
			wds.put(WIDGET_ID_3 , WIDGET_BASKETBALL);
			wds.put(WIDGET_ID_4 , WIDGET_FOOTBALL);
			wds.put(WIDGET_ID_5 , WIDGET_SOFTBALL);
		}
		
		public static WidgetInfo getInstance() {
			return wi;
		}
		
		public static String getNameById(int i ){
			return wds.get(i);
		}
		
		public static int getIdByName(String name){
			for ( int i : wds.keySet()){
				if(wds.get(i).equals(name)) return i;
			}
			return -1;
		}
		
}
