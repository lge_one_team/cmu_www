package com.oneteam.warehousesystem.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class DBConnector {

	private static final String DB_NAME = "INVENTORY";
	private static final String DB_TABLE_STATIONS_NAME = "STATIONS";
	private static final String DB_TABLE_ORDER_NAME = "ORDERS";
	//private boolean dbg = true;
	private boolean dbg = false;
	
	// JDBC driver name and database URL
	//static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	static final String DB_URL = "jdbc:mysql://localhost";
	static final String DB_INVENTORY_URL = "jdbc:mysql://localhost/INVENTORY";

	//  Database credentials
	static final String USER = "root";
	static final String PASS = "1234";
	Connection conn = null;
	Statement stmt = null;

	/*
	 * Column Info
	 * 
			String sql = "CREATE TABLE " + DB_TABLE_STATIONS_NAME + " " +
					"(id INTEGER not NULL, " +
					" is_station INTEGER, " + 
					" board_id INTEGER, " + 
					" quantity_widget1 INTEGER, " + 
					" quantity_widget2 INTEGER, " + 					
					" quantity_widget3 INTEGER, " + 					
					" quantity_widget4 INTEGER, " + 
					" quantity_widget5 INTEGER, " + 					
					" PRIMARY KEY ( id ))"; 		
	 * 
	 */

	// adapt singleton
	static DBConnector dbc = new DBConnector();

	private DBConnector() {
		initDB();
	}

	public static DBConnector getInstance() {
		return dbc;
	}

	public void initDB() {

		// DB가 있으면, db load
		if ( isDBExist(DB_NAME)){
			if ( dbg == true ) System.out.println("DB Exist. Init Complete");
			resetOrdersTable();
		}
		// db가 없으면, db create
		else {
			createDefaultDB();
			createDefaultTable();
			createDefaultOrderTable();
			insertDefaultStations();
			insertDefaultShippingDeck();
		}
	}
	
	private void createDefaultDB() {

		// TODO
		try{

			//STEP 3: Open a connection
			if ( dbg == true ) System.out.println("Connecting to database...");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);

			//STEP 4: Execute a query
			if ( dbg == true ) System.out.println("Creating database: " + DB_NAME);
			stmt = conn.createStatement();

			String sql = "CREATE DATABASE " + DB_NAME;
			stmt.executeUpdate(sql);
			if ( dbg == true ) System.out.println("Database created successfully...");
		}catch(SQLException se){
			//Handle errors for JDBC
			se.printStackTrace();
		}catch(Exception e){
			//Handle errors for Class.forName
			e.printStackTrace();
		}finally{
			//finally block used to close resources
			try{
				if(stmt!=null)
					stmt.close();
			}catch(SQLException se2){
			}// nothing we can do
			try{
				if(conn!=null)
					conn.close();
			}catch(SQLException se){
				se.printStackTrace();
			}//end finally try
		}//end try
		if ( dbg == true ) System.out.println("createDefaultDB Complete");
	}	// end createDefaultDB

	private void createDefaultTable(){

		try{
			//STEP 3: Open a connection
			if ( dbg == true ) System.out.println("Connecting to a selected database...");
			conn = DriverManager.getConnection(DB_INVENTORY_URL, USER, PASS);
			if ( dbg == true ) System.out.println("Connected database successfully...");

			//STEP 4: Execute a query
			if ( dbg == true ) System.out.println("Creating table in given database...");
			stmt = conn.createStatement();

			String sql = "CREATE TABLE " + DB_TABLE_STATIONS_NAME + " " +
					"(id INTEGER not NULL, " +
					" is_station INTEGER, " + 
					" board_id INTEGER, " + 
					" quantity_widget1 INTEGER, " + 
					" quantity_widget2 INTEGER, " + 					
					" quantity_widget3 INTEGER, " + 					
					" quantity_widget4 INTEGER, " + 
					" quantity_widget5 INTEGER, " + 					
					" PRIMARY KEY ( id ))"; 			


			stmt.executeUpdate(sql);
			if ( dbg == true ) System.out.println("Created table in given database...");
		}catch(SQLException se){
			//Handle errors for JDBC
			se.printStackTrace();
		}catch(Exception e){
			//Handle errors for Class.forName
			e.printStackTrace();
		}finally{
			//finally block used to close resources
			try{
				if(stmt!=null)
					conn.close();
			}catch(SQLException se){
			}// do nothing
			try{
				if(conn!=null)
					conn.close();
			}catch(SQLException se){
				se.printStackTrace();
			}//end finally try
		}//end try		
		if ( dbg == true ) System.out.println("createDefaultTable Complete");
	}	

	private void createDefaultOrderTable(){

		try{
			//STEP 3: Open a connection
			if ( dbg == true ) System.out.println("Connecting to a selected database...");
			conn = DriverManager.getConnection(DB_INVENTORY_URL, USER, PASS);
			if ( dbg == true ) System.out.println("Connected database successfully...");

			//STEP 4: Execute a query
			if ( dbg == true ) System.out.println("Creating table in given database...");
			stmt = conn.createStatement();

			String sql = "CREATE TABLE " + DB_TABLE_ORDER_NAME + " " +
					"(order_id INTEGER not NULL AUTO_INCREMENT, " +
					" status INTEGER, " + 
					" content VARCHAR(255), " + 
/*					" quantity_widget1 INTEGER, " + 
					" quantity_widget2 INTEGER, " + 					
					" quantity_widget3 INTEGER, " + 					
					" quantity_widget4 INTEGER, " + 
					" quantity_widget5 INTEGER, " + */					
					" PRIMARY KEY ( order_id ))"; 			


			stmt.executeUpdate(sql);
			if ( dbg == true ) System.out.println("Created table in given database...");
		}catch(SQLException se){
			//Handle errors for JDBC
			se.printStackTrace();
		}catch(Exception e){
			//Handle errors for Class.forName
			e.printStackTrace();
		}finally{
			//finally block used to close resources
			try{
				if(stmt!=null)
					conn.close();
			}catch(SQLException se){
			}// do nothing
			try{
				if(conn!=null)
					conn.close();
			}catch(SQLException se){
				se.printStackTrace();
			}//end finally try
		}//end try		
		if ( dbg == true ) System.out.println("createDefaultTable Complete");
	}	
	
	public int updateWidgetQuantityOnStation(int station_id, int widget_id, int setcount ){

		try{

			//STEP 3: Open a connection
			if ( dbg == true ) System.out.println("Connecting to a selected database...");
			conn = DriverManager.getConnection(DB_INVENTORY_URL, USER, PASS);
			if ( dbg == true ) System.out.println("Connected database successfully...");

			//STEP 4: Execute a query
			if ( dbg == true ) System.out.println("Creating statement...");
			stmt = conn.createStatement();

			String sql = "UPDATE " + DB_TABLE_STATIONS_NAME + " SET quantity_widget" + widget_id + " = " + setcount  
					+ " WHERE id = " + station_id;
			stmt.executeUpdate(sql);

		}catch(SQLException se){
			//Handle errors for JDBC
			se.printStackTrace();
		}catch(Exception e){
			//Handle errors for Class.forName
			e.printStackTrace();
		}finally{
			//finally block used to close resources
			try{
				if(stmt!=null)
					conn.close();
			}catch(SQLException se){
			}// do nothing
			try{
				if(conn!=null)
					conn.close();
			}catch(SQLException se){
				se.printStackTrace();
			}//end finally try
		}//end try		

		return 0;

	}

	public int incWidget(int station_id, int widget_id, int inc_count ){

		try{
			//STEP 3: Open a connection
			if ( dbg == true ) System.out.println("Connecting to a selected database...");
			conn = DriverManager.getConnection(DB_INVENTORY_URL, USER, PASS);
			if ( dbg == true ) System.out.println("Connected database successfully...");

			//STEP 4: Execute a query
			if ( dbg == true ) System.out.println("Creating statement...");
			stmt = conn.createStatement();
			
			
			String sql = "SELECT quantity_widget" + widget_id + " FROM " + DB_TABLE_STATIONS_NAME + " WHERE id = " + station_id;
			ResultSet rs = stmt.executeQuery(sql);
			//STEP 5: Extract data from result set
			int currentCount = 0;
			while(rs.next()){
				currentCount = rs.getInt("quantity_widget" + widget_id);
				//Display values
			if ( dbg == true ) System.out.println("[incWidget] getCurrent Count: " + currentCount);
			}
			rs.close();			

			int toSetCount = currentCount + inc_count;
			sql = "UPDATE " + DB_TABLE_STATIONS_NAME + " SET quantity_widget" + widget_id + " = " + toSetCount  
					+ " WHERE id = " + station_id;
			stmt.executeUpdate(sql);
		}catch(SQLException se){
			//Handle errors for JDBC
			se.printStackTrace();
		}catch(Exception e){
			//Handle errors for Class.forName
			e.printStackTrace();
		}finally{
			//finally block used to close resources
			try{
				if(stmt!=null)
					conn.close();
			}catch(SQLException se){
			}// do nothing
			try{
				if(conn!=null)
					conn.close();
			}catch(SQLException se){
				se.printStackTrace();
			}//end finally try
		}//end try		

		return 0;

	}

	public int decWidget(int station_id, int widget_id, int dec_count ){

		int result = -1;
		try{
			//STEP 3: Open a connection
			if ( dbg == true ) System.out.println("Connecting to a selected database...");
			conn = DriverManager.getConnection(DB_INVENTORY_URL, USER, PASS);
			if ( dbg == true ) System.out.println("Connected database successfully...");

			//STEP 4: Execute a query
			if ( dbg == true ) System.out.println("Creating statement...");
			stmt = conn.createStatement();
			
			
			String sql = "SELECT quantity_widget" + widget_id + " FROM " + DB_TABLE_STATIONS_NAME + " WHERE id = " + station_id;
			ResultSet rs = stmt.executeQuery(sql);
			//STEP 5: Extract data from result set
			int currentCount = 0;
			while(rs.next()){
				currentCount = rs.getInt("quantity_widget" + widget_id);
				//Display values
			if ( dbg == true ) System.out.println("[decWidget] getCurrent Count: " + currentCount);
			}
			rs.close();			

			int toSetCount = currentCount - dec_count;
			if ( toSetCount < 0 ){
				if ( dbg == true ) System.out.println("!!!!![decWidget]!!!!!!!!!!! You should check item counts in station");
			}
			else {
				sql = "UPDATE " + DB_TABLE_STATIONS_NAME + " SET quantity_widget" + widget_id + " = " + toSetCount  
						+ " WHERE id = " + station_id;
				stmt.executeUpdate(sql);
				result = dec_count;
			}
		}catch(SQLException se){
			//Handle errors for JDBC
			se.printStackTrace();
		}catch(Exception e){
			//Handle errors for Class.forName
			e.printStackTrace();
		}finally{
			//finally block used to close resources
			try{
				if(stmt!=null)
					conn.close();
			}catch(SQLException se){
			}// do nothing
			try{
				if(conn!=null)
					conn.close();
			}catch(SQLException se){
				se.printStackTrace();
			}//end finally try
		}//end try		

		return result;

	}	
	
	
	public Station getStationFromDB(int station_id ){

		Station st = null;
		try{

			//STEP 3: Open a connection
			if ( dbg == true ) System.out.println("Connecting to a selected database...");
			conn = DriverManager.getConnection(DB_INVENTORY_URL, USER, PASS);
			if ( dbg == true ) System.out.println("Connected database successfully...");

			//STEP 4: Execute a query
			if ( dbg == true ) System.out.println("Creating statement...");
			stmt = conn.createStatement();

			String sql = "SELECT * FROM " + DB_TABLE_STATIONS_NAME + " WHERE id = " + station_id;
			ResultSet rs = stmt.executeQuery(sql);
			//STEP 5: Extract data from result set
			while(rs.next()){
				//Retrieve by column name
				int id  = rs.getInt("id");
				int is_station = rs.getInt("is_station");
				int board_id = rs.getInt("board_id");
				int quantity_widget1 = rs.getInt("quantity_widget1");
				int quantity_widget2 = rs.getInt("quantity_widget2");				
				int quantity_widget3 = rs.getInt("quantity_widget3");				
				int quantity_widget4 = rs.getInt("quantity_widget4");				
				int quantity_widget5 = rs.getInt("quantity_widget5");

				//Display values
				if ( dbg == true ) System.out.println("[getStationFromDB] ID: " + id);
				st = new Station(id, is_station, board_id,
						quantity_widget1, quantity_widget2, quantity_widget3, quantity_widget4, quantity_widget5);

			}
			rs.close();
		}catch(SQLException se){
			//Handle errors for JDBC
			se.printStackTrace();
		}catch(Exception e){
			//Handle errors for Class.forName
			e.printStackTrace();
		}finally{
			//finally block used to close resources
			try{
				if(stmt!=null)
					conn.close();
			}catch(SQLException se){
			}// do nothing
			try{
				if(conn!=null)
					conn.close();
			}catch(SQLException se){
				se.printStackTrace();
			}//end finally try
		}//end try

		return st;
	}

	public ArrayList<Station> getAllStationInfo() {
		// TODO

		ArrayList<Station> stations = new ArrayList<Station>();

		try{
			//STEP 2: Register JDBC driver
			Class.forName("com.mysql.jdbc.Driver");

			//STEP 3: Open a connection
			if ( dbg == true ) System.out.println("Connecting to a selected database...");
			conn = DriverManager.getConnection(DB_INVENTORY_URL, USER, PASS);
			if ( dbg == true ) System.out.println("Connected database successfully...");

			//STEP 4: Execute a query
			if ( dbg == true ) System.out.println("Creating statement...");
			stmt = conn.createStatement();

			String sql = "SELECT * FROM " + DB_TABLE_STATIONS_NAME;
			ResultSet rs = stmt.executeQuery(sql);
			//STEP 5: Extract data from result set
			while(rs.next()){
				//Retrieve by column name
				int id  = rs.getInt("id");
				int is_station = rs.getInt("is_station");
				int board_id = rs.getInt("board_id");
				int quantity_widget1 = rs.getInt("quantity_widget1");
				int quantity_widget2 = rs.getInt("quantity_widget2");				
				int quantity_widget3 = rs.getInt("quantity_widget3");				
				int quantity_widget4 = rs.getInt("quantity_widget4");				
				int quantity_widget5 = rs.getInt("quantity_widget5");


				//Display values
				if ( dbg == true ) {
									System.out.print("[getAllStationInfo] get ID: " + id);
				System.out.print(", quantity_widget1: " + quantity_widget1);
				System.out.print(", quantity_widget2: " + quantity_widget2);
				System.out.print(", quantity_widget3: " + quantity_widget3);
				System.out.print(", quantity_widget4: " + quantity_widget4);
				System.out.println(", quantity_widget5: " + quantity_widget5);
					}

				stations.add(new Station(id, is_station, board_id,
						quantity_widget1, quantity_widget2, quantity_widget3, quantity_widget4, quantity_widget5));
			}
			rs.close();
		}catch(SQLException se){
			//Handle errors for JDBC
			se.printStackTrace();
		}catch(Exception e){
			//Handle errors for Class.forName
			e.printStackTrace();
		}finally{
			//finally block used to close resources
			try{
				if(stmt!=null)
					conn.close();
			}catch(SQLException se){
			}// do nothing
			try{
				if(conn!=null)
					conn.close();
			}catch(SQLException se){
				se.printStackTrace();
			}//end finally try
		}//end try

		return stations;
	}

	public boolean isDBExist(String findingDB) {
		Statement st = null;
		ResultSet rs = null;
		boolean result = false;
		try {
			Connection con = null;

			con = DriverManager.getConnection("jdbc:mysql://localhost",
					"root", "1234");


			st = con.createStatement();
			rs = st.executeQuery("SHOW DATABASES");

			if (st.execute("SHOW DATABASES")) {
				rs = st.getResultSet();
			}

			if ( dbg == true ) System.out.println("show current dbs");
			while (rs.next()) {
				String str = rs.getNString(1);
				if ( dbg == true ) System.out.println(str);
				if ( findingDB.equalsIgnoreCase(str)) {
					result = true;
					if ( dbg == true ) System.out.println("found exist db : " + findingDB);
				}
			}
		} catch (SQLException sqex) {
			if ( dbg == true ) System.out.println("SQLException: " + sqex.getMessage());
			if ( dbg == true ) System.out.println("SQLState: " + sqex.getSQLState());
		}
		finally{
			//finally block used to close resources
			try{
				if(st!=null)
					st.close();
			}catch(SQLException se2){
			}// nothing we can do
			try{
				if(conn!=null)
					conn.close();
			}catch(SQLException se){
				se.printStackTrace();
			}//end finally try
		}//end try

		return result;
	}



	public void insertDefaultStations(){

		try{
			//STEP 3: Open a connection
			if ( dbg == true ) System.out.println("Connecting to a selected database...");
			conn = DriverManager.getConnection(DB_INVENTORY_URL, USER, PASS);
			if ( dbg == true ) System.out.println("Connected database successfully...");

			//STEP 4: Execute a queryho
			if ( dbg == true ) System.out.println("Inserting records into the table...");
			stmt = conn.createStatement();

			StringBuilder sql = new StringBuilder("INSERT INTO ");
			sql.append(DB_TABLE_STATIONS_NAME).append(" ");
			// id, is_station, board_id, quantity_widget1, quantity_widget2,quantity_widget3, quantity_widget4,quantity_widget5
			sql.append("VALUES (1, 1, 0, 20, 20, 0, 0, 0)");

			if ( dbg == true ) System.out.println(sql.toString());
			stmt.executeUpdate(sql.toString());

			sql = new StringBuilder("INSERT INTO ");
			sql.append(DB_TABLE_STATIONS_NAME).append(" ");
			sql.append("VALUES (2, 1, 0, 0, 0, 20, 20, 0)");
			stmt.executeUpdate(sql.toString());

			sql = new StringBuilder("INSERT INTO ");
			sql.append(DB_TABLE_STATIONS_NAME).append(" ");
			sql.append("VALUES (3, 1, 0, 20, 0, 0, 0, 20)");
			stmt.executeUpdate(sql.toString());

			if ( dbg == true ) System.out.println("Inserted records into the table...");

		}catch(SQLException se){
			//Handle errors for JDBC
			se.printStackTrace();
		}catch(Exception e){
			//Handle errors for Class.forName
			e.printStackTrace();
		}finally{
			//finally block used to close resources
			try{
				if(stmt!=null)
					conn.close();
			}catch(SQLException se){
			}// do nothing
			try{
				if(conn!=null)
					conn.close();
			}catch(SQLException se){
				se.printStackTrace();
			}//end finally try
			if ( dbg == true ) System.out.println("createDefaultStations Complete");
		} // end finally
	}// end insertDefaultStations

	public void insertDefaultShippingDeck(){

		try{
			//STEP 3: Open a connection
			if ( dbg == true ) System.out.println("Connecting to a selected database...");
			conn = DriverManager.getConnection(DB_INVENTORY_URL, USER, PASS);
			if ( dbg == true ) System.out.println("Connected database successfully...");

			//STEP 4: Execute a query
			if ( dbg == true ) System.out.println("Inserting records into the table...");
			stmt = conn.createStatement();


			String sql = "INSERT INTO " + DB_TABLE_STATIONS_NAME + " " +
					"VALUES (4, 0, 0, NULL, NULL, NULL, NULL, NULL)";
			stmt.executeUpdate(sql);			

			if ( dbg == true ) System.out.println("Inserted records into the table...");

		}catch(SQLException se){
			//Handle errors for JDBC
			se.printStackTrace();
		}catch(Exception e){
			//Handle errors for Class.forName
			e.printStackTrace();
		}finally{
			//finally block used to close resources
			try{
				if(stmt!=null)
					conn.close();
			}catch(SQLException se){
			}// do nothing
			try{
				if(conn!=null)
					conn.close();
			}catch(SQLException se){
				se.printStackTrace();
			}//end finally try
			if ( dbg == true ) System.out.println("insertDefaultShippingDeck Complete");
		} // end finally
	}// end insertDefaultStations

	public int updateOrderStatus(int order_id, int status ) {

		try{

			//STEP 3: Open a connection
			if ( dbg == true ) System.out.println("Connecting to a selected database...");
			conn = DriverManager.getConnection(DB_INVENTORY_URL, USER, PASS);
			if ( dbg == true ) System.out.println("Connected database successfully...");

			//STEP 4: Execute a query
			if ( dbg == true ) System.out.println("Creating statement...");
			stmt = conn.createStatement();

			String sql = "UPDATE " + DB_TABLE_ORDER_NAME + " SET status = " + status  
					+ " WHERE order_id = " + order_id;
			stmt.executeUpdate(sql);

		}catch(SQLException se){
			//Handle errors for JDBC
			se.printStackTrace();
		}catch(Exception e){
			//Handle errors for Class.forName
			e.printStackTrace();
		}finally{
			//finally block used to close resources
			try{
				if(stmt!=null)
					conn.close();
			}catch(SQLException se){
			}// do nothing
			try{
				if(conn!=null)
					conn.close();
			}catch(SQLException se){
				se.printStackTrace();
			}//end finally try
		}//end try		

		return 0;
		
		
	}

	public int insertNewOrder(int set_Status, String content) {

		int order_id = -1;
		
		try{
			//STEP 3: Open a connection
			if ( dbg == true ) System.out.println("Connecting to a selected database...");
			conn = DriverManager.getConnection(DB_INVENTORY_URL, USER, PASS);
			if ( dbg == true ) System.out.println("Connected database successfully...");

			//STEP 4: Execute a query
			if ( dbg == true ) System.out.println("Inserting records into the table...");
			stmt = conn.createStatement();


			String sql = "INSERT INTO " + DB_TABLE_ORDER_NAME + " " +
					"VALUES (null, " + set_Status + ",'" + content + "')";
			stmt.executeUpdate(sql);	
			if ( dbg == true ) System.out.println("Inserted records into the table...");
			
			
			/****************************************************************************/
			
			// STEP 5: Get order_id from db
			sql = "SELECT LAST_INSERT_ID()";
			ResultSet rs;
			
			
			rs = stmt.executeQuery(sql);
			if (stmt.execute(sql)) {
				rs = stmt.getResultSet();
			}
			
			if ( dbg == true ) System.out.println("SELECT LAST_INSERT_ID()");
			while (rs.next()) {
				//String str = rs.getNString(1);
				order_id = rs.getInt(1);
				
				if ( dbg == true ) System.out.println("[insertNewOrder] order_id: " + order_id);
			}
			rs.close();	

		}catch(SQLException se){
			//Handle errors for JDBC
			se.printStackTrace();
		}catch(Exception e){
			//Handle errors for Class.forName
			e.printStackTrace();
		}finally{
			//finally block used to close resources
			try{
				if(stmt!=null)
					conn.close();
			}catch(SQLException se){
			}// do nothing
			try{
				if(conn!=null)
					conn.close();
			}catch(SQLException se){
				se.printStackTrace();
			}//end finally try
			if ( dbg == true ) System.out.println("insertDefaultShippingDeck Complete");
		} // end finally
		
		return order_id;
	}

	public Order getOrderFromDB(int id) {
		Order order = null;
		try{
			//STEP 2: Register JDBC driver
			Class.forName("com.mysql.jdbc.Driver");

			//STEP 3: Open a connection
			if ( dbg == true ) System.out.println("Connecting to a selected database...");
			conn = DriverManager.getConnection(DB_INVENTORY_URL, USER, PASS);
			if ( dbg == true ) System.out.println("Connected database successfully...");

			//STEP 4: Execute a query
			if ( dbg == true ) System.out.println("Creating statement...");
			stmt = conn.createStatement();

			String sql = "SELECT * FROM " + DB_TABLE_ORDER_NAME + " WHERE order_id = " + id;
			ResultSet rs = stmt.executeQuery(sql);
			//STEP 5: Extract data from result set
			while(rs.next()){
				//Retrieve by column name
				int order_id  = rs.getInt("order_id");
				int status = rs.getInt("status");
				String content = rs.getString("content");

				//Display values
				if ( dbg == true ) {
									System.out.print("[getOrderFromDB] get ID: " + order_id);
				System.out.print(", status: " + status);
				System.out.println(", content: " + content);
					}

				order = new Order(order_id, status, content);
			}
			rs.close();
		}catch(SQLException se){
			//Handle errors for JDBC
			se.printStackTrace();
		}catch(Exception e){
			//Handle errors for Class.forName
			e.printStackTrace();
		}finally{
			//finally block used to close resources
			try{
				if(stmt!=null)
					conn.close();
			}catch(SQLException se){
			}// do nothing
			try{
				if(conn!=null)
					conn.close();
			}catch(SQLException se){
				se.printStackTrace();
			}//end finally try
		}//end try

		return order;
	}

	public ArrayList<Order> getAllOrders() {
		ArrayList<Order> orders = new ArrayList<Order>();

		try{
			//STEP 2: Register JDBC driver
			Class.forName("com.mysql.jdbc.Driver");

			//STEP 3: Open a connection
			if ( dbg == true ) System.out.println("Connecting to a selected database...");
			conn = DriverManager.getConnection(DB_INVENTORY_URL, USER, PASS);
			if ( dbg == true ) System.out.println("Connected database successfully...");

			//STEP 4: Execute a query
			if ( dbg == true ) System.out.println("Creating statement...");
			stmt = conn.createStatement();

			String sql = "SELECT * FROM " + DB_TABLE_ORDER_NAME;
			ResultSet rs = stmt.executeQuery(sql);
			//STEP 5: Extract data from result set
			while(rs.next()){
				//Retrieve by column name
				int order_id  = rs.getInt("order_id");
				int status = rs.getInt("status");
				String content = rs.getString("content");

				//Display values
				if ( dbg == true ) {
									System.out.print("[getAllOrders] get ID: " + order_id);
				System.out.print(", status: " + status);
				System.out.println(", content: " + content);
					}

				orders.add(new Order(order_id, status, content));
			}
			rs.close();
		}catch(SQLException se){
			//Handle errors for JDBC
			se.printStackTrace();
		}catch(Exception e){
			//Handle errors for Class.forName
			e.printStackTrace();
		}finally{
			//finally block used to close resources
			try{
				if(stmt!=null)
					conn.close();
			}catch(SQLException se){
			}// do nothing
			try{
				if(conn!=null)
					conn.close();
			}catch(SQLException se){
				se.printStackTrace();
			}//end finally try
		}//end try

		return orders;
	}

	public void deleteStationsTable() {
		// TODO Auto-generated method stub
		try{
		      //STEP 2: Register JDBC driver
		      Class.forName("com.mysql.jdbc.Driver");

		      //STEP 3: Open a connection
				if ( dbg == true ) System.out.println("Connecting to a selected database...");
				conn = DriverManager.getConnection(DB_INVENTORY_URL, USER, PASS);
				if ( dbg == true ) System.out.println("Connected database successfully...");
		      
		      //STEP 4: Execute a query
		      System.out.println("Deleting table in given database...");
		      stmt = conn.createStatement();
		      
		      String sql = "DROP TABLE " + DB_TABLE_STATIONS_NAME;
		 
		      stmt.executeUpdate(sql);
		      System.out.println("Table  deleted :" + DB_TABLE_STATIONS_NAME);
		   }catch(SQLException se){
		      //Handle errors for JDBC
		      se.printStackTrace();
		   }catch(Exception e){
		      //Handle errors for Class.forName
		      e.printStackTrace();
		   }finally{
		      //finally block used to close resources
		      try{
		         if(stmt!=null)
		            conn.close();
		      }catch(SQLException se){
		      }// do nothing
		      try{
		         if(conn!=null)
		            conn.close();
		      }catch(SQLException se){
		         se.printStackTrace();
		      }//end finally try
		   }//end try		
		
	}

	public void deleteOrdersTable() {

		try{
			      //STEP 2: Register JDBC driver
			      Class.forName("com.mysql.jdbc.Driver");

			      //STEP 3: Open a connection
					if ( dbg == true ) System.out.println("Connecting to a selected database...");
					conn = DriverManager.getConnection(DB_INVENTORY_URL, USER, PASS);
					if ( dbg == true ) System.out.println("Connected database successfully...");
			      
			      //STEP 4: Execute a query
			      System.out.println("Deleting table in given database...");
			      stmt = conn.createStatement();
			      
			      String sql = "DROP TABLE " + DB_TABLE_ORDER_NAME;
			 
			      stmt.executeUpdate(sql);
			      System.out.println("Table  deleted :" + DB_TABLE_ORDER_NAME);
			   }catch(SQLException se){
			      //Handle errors for JDBC
			      se.printStackTrace();
			   }catch(Exception e){
			      //Handle errors for Class.forName
			      e.printStackTrace();
			   }finally{
			      //finally block used to close resources
			      try{
			         if(stmt!=null)
			            conn.close();
			      }catch(SQLException se){
			      }// do nothing
			      try{
			         if(conn!=null)
			            conn.close();
			      }catch(SQLException se){
			         se.printStackTrace();
			      }//end finally try
			   }//end try		
		
	}

	public void resetAllTables() {
		resetStationsTable();
		resetOrdersTable();
		
	}

	public void resetStationsTable() {
		deleteStationsTable();
		createDefaultTable();
		insertDefaultStations();
		insertDefaultShippingDeck();
	}

	public void resetOrdersTable() {
		deleteOrdersTable();
		createDefaultOrderTable();
	}

	public void deleteDB() {
		try{
		      //STEP 2: Register JDBC driver
		      Class.forName("com.mysql.jdbc.Driver");

		      //STEP 3: Open a connection
		      System.out.println("Connecting to a selected database...");
		      conn = DriverManager.getConnection(DB_URL, USER, PASS);
		      System.out.println("Connected database successfully...");
		      
		      //STEP 4: Execute a query
		      System.out.println("Deleting database...");
		      stmt = conn.createStatement();
		      
		      String sql = "DROP DATABASE " + DB_NAME;
		      stmt.executeUpdate(sql);
		      System.out.println("Database deleted successfully...");
		   }catch(SQLException se){
		      //Handle errors for JDBC
		      se.printStackTrace();
		   }catch(Exception e){
		      //Handle errors for Class.forName
		      e.printStackTrace();
		   }finally{
		      //finally block used to close resources
		      try{
		         if(stmt!=null)
		            conn.close();
		      }catch(SQLException se){
		      }// do nothing
		      try{
		         if(conn!=null)
		            conn.close();
		      }catch(SQLException se){
		         se.printStackTrace();
		      }//end finally try
		   }//end try		
	}
	
	
}
