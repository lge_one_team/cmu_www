package com.oneteam.warehousesystem.db;

import java.util.HashMap;

public class Order {
	
	public final int STATUS_PENDING = 0;
	public final int STATUS_IN_PROCESSING = 1;
	public final int STATUS_BACK_ORDER = 2;
	public final int STATUS_COMPLETE =3;
	
	public final String STATUS_PENDING_STRING = "Pending";
	public final String STATUS_IN_PROCESSING_STRING = "Processing";
	public final String STATUS_BACK_ORDER_STRING = "BackOrder";
	public final String STATUS_COMPLETE_STRING ="Complete";
	
	private int order_id;
	private int status;
	private String content = null;
	
/*	public int item1_request_cnt = 0;
	public int item2_request_cnt = 0;
	public int item3_request_cnt = 0;
	public int item4_request_cnt = 0;
	public int item5_request_cnt = 0;*/
	
	public static HashMap<Integer, String> order_hashmap = new HashMap<Integer, String>();
	
	public void init() {
		order_hashmap.put(STATUS_PENDING, STATUS_PENDING_STRING);
		order_hashmap.put(STATUS_IN_PROCESSING, STATUS_IN_PROCESSING_STRING);
		order_hashmap.put(STATUS_BACK_ORDER, STATUS_BACK_ORDER_STRING);
		order_hashmap.put(STATUS_COMPLETE, STATUS_COMPLETE_STRING);		
	}
	

	
	public Order(int status, String content) {
		super();
		this.status = status;
		this.content = content;
		init();
	}



	public Order(int order_id, int status, String content) {
		super();
		this.order_id = order_id;
		this.status = status;
		this.content = content;
		init();
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		StringBuilder sb = new StringBuilder();
		sb.append("order_id: ").append(order_id).append("  ");
		sb.append("status: ").append(order_hashmap.get(status)).append("  ");
		sb.append("content: ").append(content);		
		return sb.toString();
	}



	public int getOrder_id() {
		return order_id;
	}



	public void setOrder_id(int order_id) {
		this.order_id = order_id;
	}



	public int getStatus() {
		return status;
	}



	public void setStatus(int status) {
		this.status = status;
	}



	public String getContent() {
		return content;
	}



	public void setContent(String content) {
		this.content = content;
	}
	
	
	
}
