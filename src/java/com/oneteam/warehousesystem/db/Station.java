package com.oneteam.warehousesystem.db;


public class Station {

	// variables
	private int station_id;
	private int is_station;
	
	public String item1_name = null;
	public String item2_name = null;
	public String item3_name = null;
	public String item4_name = null;
	public String item5_name = null;
	
	public int item1_cnt = 0;
	public int item2_cnt = 0;
	public int item3_cnt = 0;
	public int item4_cnt = 0;
	public int item5_cnt = 0;	
	
	private int board_id =0;
	
	public int getWidgetCount(int widget_id){
		switch ( widget_id ){
		case 1:
			return item1_cnt;
		case 2:
			return item2_cnt;
		case 3:
			return item3_cnt;
		case 4:
			return item4_cnt;
		case 5:
			return item5_cnt;
		default :
			System.out.println("[getWidgetCount] Error! widget id is invalid");
		}
		return -1;
	}

	public Station(int station_id, int is_station, int board_id,
			int item1_cnt, int item2_cnt, int item3_cnt, int item4_cnt, int item5_cnt) {
		super();
		this.station_id = station_id;
		this.is_station = is_station;
		this.board_id = board_id;
		this.item1_name = WidgetInfo.getNameById(WidgetInfo.WIDGET_ID_1);
		this.item2_name = WidgetInfo.getNameById(WidgetInfo.WIDGET_ID_2);
		this.item3_name = WidgetInfo.getNameById(WidgetInfo.WIDGET_ID_3);
		this.item4_name = WidgetInfo.getNameById(WidgetInfo.WIDGET_ID_4);
		this.item5_name = WidgetInfo.getNameById(WidgetInfo.WIDGET_ID_5);
		this.item1_cnt = item1_cnt;
		this.item2_cnt = item2_cnt;
		this.item3_cnt = item3_cnt;
		this.item4_cnt = item4_cnt;
		this.item5_cnt = item5_cnt;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		StringBuilder sb = new StringBuilder();
		sb.append("id:").append(station_id).append(", ");
		sb.append("is_station:").append(is_station).append(", ");
		sb.append("item1_name:").append(item1_name).append(", ");
		sb.append("item1_cnt:").append(item1_cnt).append(", ");
		sb.append("item2_name:").append(item2_name).append(", ");
		sb.append("item2_cnt:").append(item2_cnt).append(", ");
		sb.append("item3_name:").append(item3_name).append(", ");
		sb.append("item3_cnt:").append(item3_cnt).append(", ");
		sb.append("item4_name:").append(item4_name).append(", ");
		sb.append("item4_cnt:").append(item4_cnt).append(", ");
		sb.append("item5_name:").append(item5_name).append(", ");
		sb.append("item5_cnt:").append(item5_cnt).append(", ");		
		
		return sb.toString();
	}
	
	
	
}
