package com.oneteam.warehousesystem.db;

import java.util.ArrayList;

public interface DBConnBase {
	
	abstract public void initDB();
	abstract public int updateWidgetQuantityOnStation(int station_id, int widget_id, int setcount );
	abstract public int incWidget(int station_id, int widget_id, int inc_count );
	abstract public int decWidget(int station_id, int widget_id, int dec_count );
	abstract public Station getStationFromDB(int station_id );
	abstract public ArrayList<Station> getAllStationInfo();
	abstract public boolean isDBExist(String findingDB);
	
	abstract public int insertNewOrder(int set_Status, String content );
	abstract public int updateOrderStatus(int order_id, int set_Status );
	abstract public Order getOrderFromDB(int order_id );
	abstract public ArrayList<Order> getAllOrders();
	
	abstract public void deleteStationsTable();
	abstract public void deleteOrdersTable();
	abstract public void resetStationsTable();
	abstract public void resetOrdersTable();
	abstract public void resetAllTables();
	
	abstract public void deleteDB();

		
}
