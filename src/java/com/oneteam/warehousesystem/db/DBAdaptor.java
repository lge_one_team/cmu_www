package com.oneteam.warehousesystem.db;

import java.util.ArrayList;

public class DBAdaptor implements DBConnBase {

	DBConnector dbc = DBConnector.getInstance();
	
	public void initDB() {
		dbc.initDB();
	}

	public int updateWidgetQuantityOnStation(int station_id, int widget_id,	int setcount) {
		return dbc.updateWidgetQuantityOnStation(station_id, widget_id, setcount);
	}

	public int incWidget(int station_id, int widget_id, int inc_count) {
		return dbc.incWidget(station_id, widget_id, inc_count);
	}

	public int decWidget(int station_id, int widget_id, int dec_count) {
		return dbc.decWidget(station_id, widget_id, dec_count);
	}

	public Station getStationFromDB(int station_id) {
		return dbc.getStationFromDB(station_id);
	}

	public ArrayList<Station> getAllStationInfo() {
		return dbc.getAllStationInfo();
	}

	public boolean isDBExist(String findingDB) {
		return dbc.isDBExist(findingDB);
	}

	public int insertNewOrder(int set_Status, String content) {
		return dbc.insertNewOrder(set_Status, content);
	}

	public int updateOrderStatus(int order_id, int set_Status) {
		return dbc.updateOrderStatus(order_id, set_Status);
	}

	public Order getOrderFromDB(int order_id) {
		return dbc.getOrderFromDB(order_id);
	}

	public ArrayList<Order> getAllOrders() {
		return dbc.getAllOrders();
	}

	public void deleteStationsTable() {
		dbc.deleteStationsTable();
	}

	public void deleteOrdersTable() {
		dbc.deleteOrdersTable();
	}

	public void resetStationsTable() {
		dbc.resetStationsTable();
	}

	public void resetOrdersTable() {
		dbc.resetOrdersTable();
	}

	public void resetAllTables() {
		dbc.resetAllTables();
	}

	public void deleteDB() {
		dbc.deleteDB();
	}

}
