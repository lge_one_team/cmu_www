package com.oneteam.warehousesystem.db.test;

import java.util.ArrayList;

import com.oneteam.warehousesystem.DBManager;
import com.oneteam.warehousesystem.db.DBAdaptor;
import com.oneteam.warehousesystem.db.DBConnBase;
import com.oneteam.warehousesystem.db.DBConnector;
import com.oneteam.warehousesystem.db.Order;
import com.oneteam.warehousesystem.db.Station;
import com.oneteam.warehousesystem.db.WidgetInfo;

public class testDB {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		DBManager dbc = DBManager.getInstance();
		//DBConnBase dbc = DBConnector.getInstance();
		//dbc.createDefaultDB();
		//dbc.createDefaultTable();
		//dbc.insertDefaultStations();
		//dbc.insertDefaultShippingDeck();
		//System.out.println("db? :" + dbc.isDBExist("INVENTORY"));
		//dbc.initDB();
		//dbc.deleteDB();
		
		//dbc.deleteOrdersTable();
		//dbc.deleteStationsTable();
		
		
		/*
		dbc.updateOrderStatus(24, 2);
		dbc.updateOrderStatus(23, 2);
		
		ArrayList<Order> orders = dbc.getAllOrders();
		for ( Order order : orders ){
			System.out.println(order.toString());
		}
		
		System.out.println(dbc.getOrderFromDB(26).toString());
		System.out.println(dbc.getOrderFromDB(20).toString());
		System.out.println(dbc.getOrderFromDB(18).toString());
		*/

		int order_id = dbc.insertNewOrder(1, "test order 1");
		System.out.println("insert order is ok. Order id :" + order_id);
		
		order_id = dbc.insertNewOrder(2, "test order 2");
		System.out.println("insert order is ok. Order id :" + order_id);
		
		order_id = dbc.insertNewOrder(3, "test order 3");
		System.out.println("insert order is ok. Order id :" + order_id);
		
		order_id = dbc.insertNewOrder(1, "test order 1");
		System.out.println("insert order is ok. Order id :" + order_id);
		
		order_id = dbc.insertNewOrder(2, "test order 2");
		System.out.println("insert order is ok. Order id :" + order_id);
		
		order_id = dbc.insertNewOrder(3, "test order 3");
		System.out.println("insert order is ok. Order id :" + order_id);

		System.out.println(dbc.getOrderFromDB(2).toString());
		System.out.println(dbc.getOrderFromDB(3).toString());
		System.out.println(dbc.getOrderFromDB(4).toString());
		
		ArrayList <Station> sts = dbc.getAllStationInfo();
		for ( Station st : sts ) {
			System.out.println( st.toString());
		}
		
		System.out.println(dbc.getStationFromDB(1).toString());
		System.out.println(dbc.getStationFromDB(2).toString());
		System.out.println(dbc.getStationFromDB(3).toString());
		
		dbc.updateWidgetQuantityOnStation(1, 1, 77);
		dbc.updateWidgetQuantityOnStation(2, 3, 99);
		dbc.updateWidgetQuantityOnStation(3, 2, 88);
				
		dbc.incWidget(1, 3, 5);
		dbc.incWidget(2, 3, 5);
		dbc.incWidget(3, 5, 10);
		dbc.decWidget(1, 1, 20);

		sts = dbc.getAllStationInfo();
		for ( Station st : sts ) {
			System.out.println( st.toString());
		}
		
		dbc.getAllWidgetInfo();
		
		dbc.getAllWidgetInfo(true);
		dbc.getAllWidgetInfo(false);
		
		System.out.println("WIDGET_BASEBALL on station 1:" + dbc.getItemsAtStation(1, WidgetInfo.WIDGET_BASEBALL));
		System.out.println("WIDGET_BASKETBALL on station 2:" + dbc.getItemsAtStation(2, WidgetInfo.WIDGET_BASKETBALL));
		System.out.println("WIDGET_SOCKERBALL on station 3:" + dbc.getItemsAtStation(3, WidgetInfo.WIDGET_SOCKERBALL));
		
	}

}
