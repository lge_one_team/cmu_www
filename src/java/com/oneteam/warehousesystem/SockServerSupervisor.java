package com.oneteam.warehousesystem;

import java.util.StringTokenizer;

public class SockServerSupervisor extends SockServer {
	WarehouseManager whMngr;
	
	public SockServerSupervisor (WarehouseManager whMngr) {
		super (2222, 1);
		this.whMngr = whMngr;
	}
		
	@Override
	public void rcvpkt (String rcvpkt) {
		StringTokenizer st = new StringTokenizer (rcvpkt, ",");
		String          s  = st.nextToken ();

		System.out.println ("SockServerSupervisor get a packet : " + rcvpkt);
        if (s.equals ("SW1")) {
			DBManager dbmngr = DBManager.getInstance ();
			String    dbinfo = dbmngr.getAllItems ();
			
			if (dbinfo != null) {
				mkandsndpkt ("WS1", dbinfo);
			}
        } else if (s.equals ("SW2")) {
			while (st.hasMoreTokens ()) {
				String wn = st.nextToken ();

				if (wn.equals ("E")) {
					//
					break;
				} else {
					int       station_id, inc_count;
					String    widget_name;
					DBManager dbmngr = DBManager.getInstance ();
					
					station_id  = Integer.parseInt (wn);
					widget_name = st.nextToken ();
					inc_count   = Integer.parseInt(st.nextToken ());
					dbmngr.incWidget (station_id, widget_name, inc_count);
				}
            }
        	mkandsndpkt ("WS2", null);
        } else if (s.equals ("SW3")) {
        	mkandsndpkt ("WS3", whMngr.getStatusFacility ());
        } else if (s.equals ("SW4")) {
			DBManager dbmngr = DBManager.getInstance ();
        	mkandsndpkt ("WS4", dbmngr.getAllOrderItem ());
        } else if (s.equals ("SW5")) {
			DBManager dbmngr = DBManager.getInstance ();
        	mkandsndpkt ("WS5", dbmngr.getOrderItem (Integer.parseInt(st.nextToken ())));
		} else {
			System.out.println ("but it is unKnown protocol ");
		}
	}
	
	public void updateSomething (String strWhat) {
    	mkandsndpkt ("WS0", strWhat);
	}
}
