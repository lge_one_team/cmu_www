/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.oneteam.customerordersystem;

import java.util.Objects;

/**
 *
 * @author haesun.kim
 */
class Widget {
    private String name;
    private int quantity;
    
    public Widget(String name, int quantity) {
        this.name = name;
        this.quantity = quantity;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getName() {
        return name;
    }

    public int getQuantity() {
        return quantity;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 19 * hash + Objects.hashCode(this.name);
        hash = 19 * hash + this.quantity;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Widget other = (Widget) obj;
        return true;
    }

    @Override
    public String toString() {
        return "<" + name + "> Widget : "+ quantity;
    }

}
