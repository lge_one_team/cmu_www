/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oneteam.customerordersystem;

import java.util.ArrayList;

/**
 *
 * @author haesun.kim
 */
public class WidgetDAO {

    private ArrayList<Widget> list;

    public WidgetDAO() {
        list = new ArrayList<Widget>();
    }


    public void setList(ArrayList<Widget> list) {
        this.list = list;
    }

    public ArrayList<Widget> getList() {
        return list;
    }


    public void showList() {
        for (Widget w : list) {
            System.out.println(w);
        }
    }


    public void addWidget(String name, int quantity) {
        list.add(new Widget(name, quantity));
    }


    public void updateWidget(String name, int quantity) {
        Widget w = search(name);
        if (w != null) {
            w.setName(name);
            w.setQuantity(quantity);
        }
    }


    public Widget search(String name) {
        for (Widget w : list) {
            String nm = w.getName();
            if (nm.equals(name)) {
                return w;
            }
        }
        return null;
    }


    public void deleteWidget(String name) {
        Widget w = search(name);
        if (w != null) {
            list.remove(w);
        }
    }
    
    public void deleteWidgetAll() {
    	list.removeAll(list);
    }

    public static void main(String[] args) {
        WidgetDAO dao = new WidgetDAO();
        dao.addWidget("baseball", 5);
        dao.addWidget("basketball", 3);
        dao.showList();
        System.out.println("================");

        Widget w = dao.search("baseball");
        System.out.println(w);
        System.out.println("================");

        dao.updateWidget("basketball", 10);
        dao.showList();
        System.out.println("================");

        dao.deleteWidget("basketball");
        dao.showList();
        System.out.println("================");
    }
}
