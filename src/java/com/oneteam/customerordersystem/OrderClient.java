/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.oneteam.customerordersystem;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 *
 * @author haesun.kim
 */
public class OrderClient {

	private static OrderClient instance;
	// Request
    private static final String MSG_ID_WIDGETLIST_REQ = "CW1";
    private static final String MSG_ID_ORDER_REQ = "CW2";
    // Response
    private static final String MSG_ID_WIDGETLIST_RES = "WC1";
    private static final String MSG_ID_ORDER_RES = "WC2";  
    
    private static final String END = "E";
    
    private static final String SPLIT = ",";
    ArrayList<WidgetList> wl;
    
    String ip = "127.0.0.1";
    int port = 1111;
    Socket s = null;
    ObjectInputStream ois;
    ObjectOutputStream oos;
	OrderGUI orderGUI;
    WidgetDAO widgetDao;
	
    private OrderClient() {
        wl = new ArrayList<WidgetList>();
        widgetDao = new WidgetDAO();
    }

    public static OrderClient getInstance(){
         if(instance == null) {
              instance = new OrderClient();
         }
         return instance;
    }
    
    public boolean vaildWidgetName(String name) {
    	System.out.println("input widget name :"+name);
        for (WidgetList w : wl) {
            String nm = w.getName();
            System.out.println("listed widget name:"+nm);
            if (nm.equals(name)) {
                return true;
            }
        }
        return false;
    }

    public boolean connect() {
        try {
 			s = new Socket(ip, port);
        	System.out.println("1.Socket");
            ois = new ObjectInputStream(s.getInputStream());
            oos = new ObjectOutputStream(s.getOutputStream());
            System.out.println("2.Stream");
            new OrderDAOThread().start();
            System.out.println("3.OrderDAOThread");
        } catch (Exception e) {
        	e.printStackTrace();
            System.out.println("Connection Exception");
            return false;
        }
        return true;
    }
//    public void disconnect() {
//    	try {
//    		ois.close();
//    		oos.close();
//			//s.close();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//    }

    void requestWidgetList() {
        StringBuilder msg = new StringBuilder();
       	if(s == null) {
       		if(!connect()) {
       			orderGUI.displayConnectionError();
       			return;
       		}
    	}
        try {
            msg.append(MSG_ID_WIDGETLIST_REQ);
            msg.append(SPLIT);
            msg.append(END);
            oos.writeObject(msg.toString());
            oos.flush();
        } catch (IOException ioe) {
        	ioe.printStackTrace();
            System.out.println("IOException");
        }
    }

    class OrderDAOThread extends Thread {

        public void run() {
            try {
                while (true) {
                    String msg = (String) ois.readObject();
                    System.out.println("Client : "+msg);
                    
                    StringTokenizer st = new StringTokenizer(msg, SPLIT);
                    String s = st.nextToken();
                    if(s.equals(MSG_ID_WIDGETLIST_RES)){
                        System.out.println("I got M1 message");
                        parseWidgetList(msg);
                        orderGUI.showWidgetList();
                    }else if(s.equals(MSG_ID_ORDER_RES)){
                        System.out.println("Order Complete");
                    }                    
//                	disconnect();
                }
            } catch (Exception e) {
            	e.printStackTrace();
                System.out.println(e);
            }
        }

    }
	public void startscustomersystem() {
		orderGUI = new OrderGUI();
		
		orderGUI.setVisible(true);

		System.out.println("[Customer] GUI setVisible");    	

       	if(s == null) {
       		connect();
    	}
	
       	requestWidgetList();
	}

    public void parseWidgetList(String msg) {
        StringTokenizer st = new StringTokenizer(msg, SPLIT);
        if(!st.nextToken().equals(MSG_ID_WIDGETLIST_RES)) return;
        while(st.hasMoreTokens()){
            String name = st.nextToken();
            if(name.equals(END)) {
            	return;
            }
            String quantity = st.nextToken();
            int num = Integer.parseInt(quantity);
            wl.add(new WidgetList(name, num));
        }
	}

	public void sendOrder() {
        StringBuilder msg = new StringBuilder();
       	if(s == null) {
       		connect();
    	}
        try {
            msg.append(MSG_ID_ORDER_REQ);
            for (Widget w : widgetDao.getList()) {
            	msg.append(SPLIT);
                msg.append(w.getName());
                msg.append(SPLIT);
                msg.append(w.getQuantity());
            }
            msg.append(SPLIT);
            msg.append(END);
            oos.writeObject(msg.toString());
            System.out.println(msg);
            oos.flush();
        } catch (IOException ioe) {
        	ioe.printStackTrace();
            System.out.println("IOException");
        }
     	widgetDao.deleteWidgetAll();
    }

	public void addWidget(String name, int num) {
		widgetDao.addWidget(name, num);		
	}

	public void deleteWidget(String name) {
		widgetDao.deleteWidget(name);	
	}

	public void updateWidget(String name, int num) {
		widgetDao.updateWidget(name, num);
	}

	public void deleteWidgetAll() {
		widgetDao.deleteWidgetAll();		
	}

	public ArrayList<Widget> getList() {
		return widgetDao.getList();
	}
}
