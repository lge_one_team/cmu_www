package com.oneteam.facilitymanager;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;

public class ShippingManager extends Observable implements Observer, DebugUtil {

	private int currentStation;
	private int nextStation;
	private int prevStation;

	private int targetStation;

	private boolean debugEnabled = true;

	private RobotClient robotClient;
	private StationClient stationClient;

	// private RobotStatus robotStatus;
	// private StationStatus stationStatus;

	private ShippingThread shippingThread = null;

	private static final int POLLING_PERIOD = 1000; // 1000 ms

	class ShippingThread extends Thread {
		public void run() {

			boolean robotStatus = false;

			if (orderList == null) {
				return;
			}

			print("Ready to ship");

			currentStation = stationClient.getRobotPositionEx();
			prevStation = currentStation;

			print("Ship Progress");

			// 1. goto initial position INVENTORY_1

			if (currentStation != StationStatus.STATION_STATUS_POSITION_INVENTORY_1) {
				
				robotClient.gotoNextStationEx();

				print("currentPos : " + currentStation);

				while (currentStation != StationStatus.STATION_STATUS_POSITION_INVENTORY_1) {

					robotStatus = robotClient.getStatusEx();
					currentStation = stationClient.getRobotPositionEx();

					if (robotStatus == true) {
						if (stationClient.getRobotPositionEx() != StationStatus.STATION_STATUS_POSITION_INVENTORY_1) {
							robotClient.gotoNextStationEx();
						}
					}

					print("Goto INVENTORY_1 (current pos : " + currentStation
							+ " )");

					// Update prev. station
					if ((currentStation != prevStation)
							&& (currentStation != StationStatus.STATION_STATUS_POSITION_UNKNOW)) {
						// robotClient.gotoNextStationEx();
						prevStation = currentStation;
					}

					try {
						sleep(POLLING_PERIOD);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}

			// goto 1st station
			for (int station = StationStatus.STATION_STATUS_POSITION_INVENTORY_1; station <= StationStatus.STATION_STATUS_POSITION_INVENTORY_3; ++station) {

				int numOfWidget = getWidget(station, orderList);

				print("Load station #" + station);

				if (numOfWidget != 0) {
					while (true) {
						int workerStation = stationClient
								.confirmWorkerCompleteEx();

						if (workerStation == station) {
							setLoadComplete(station, orderList);
							break;
						}

						try {
							sleep(POLLING_PERIOD);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}

				// 1. goto initial position INVENTORY_1
				robotClient.gotoNextStationEx();

				targetStation = station + 1;
				currentStation = stationClient.getRobotPositionEx();

				while (currentStation != targetStation) {

					robotStatus = robotClient.getStatusEx();
					currentStation = stationClient.getRobotPositionEx();
					
					if (robotStatus == true) {
						if (stationClient.getRobotPositionEx() != targetStation) {
							robotClient.gotoNextStationEx();
						}
					}

					print("wait for robot moving completion.. cur :"
							+ currentStation + ": " + targetStation);
					try {
						sleep(POLLING_PERIOD);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}

			while (true) {

				int workerStation = stationClient.confirmWorkerCompleteEx();

				if (workerStation == StationStatus.STATION_STATUS_POSITION_SHIPPING) {
					// completed
					notifyToFacilityManager();
					break;
				}

				try {
					sleep(POLLING_PERIOD);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}
	}

	private static ShippingManager instance = null;

	private ArrayList<OrderWidget> orderList;

	private int getWidget(int station, ArrayList<OrderWidget> orderList) {
		int numOfWidget = 0;

		for (Iterator<OrderWidget> iterator = orderList.iterator(); iterator
				.hasNext();) {
			OrderWidget orderWidget = (OrderWidget) iterator.next();

			if (orderWidget.getStationId() == station) {
				numOfWidget += orderWidget.getNumOfWidget();
			}

		}
		return numOfWidget;
	}

	private void setLoadComplete(int station, ArrayList<OrderWidget> orderList) {
		for (Iterator<OrderWidget> iterator = orderList.iterator(); iterator
				.hasNext();) {
			OrderWidget orderWidget = (OrderWidget) iterator.next();

			if (orderWidget.getStationId() == station) {
				orderWidget.setLoaded(true);
			}
		}
	}

	private ShippingManager() {
		// robotStatus = RobotStatus.getInstance();
		// stationStatus = StationStatus.getInstance();
		setCurrentStation(0);
		setNextStation(0);
		setPrevStation(0);
	}

	public static ShippingManager getInstance() {
		if (instance == null) {
			instance = new ShippingManager();
		}
		return instance;
	}

	// Private APIs
	private void notifyToFacilityManager() {
		print("notifyToFacilityManager()");
		this.setChanged();
		this.notifyObservers(this);
		shippingThread = null;
	}

	// Public APIs
	void ShipAnOrder(ArrayList<OrderWidget> orderList) {

		this.orderList = orderList;

		this.robotClient = RobotClient.getInstance();
		this.stationClient = StationClient.getInstance();

		this.robotClient.connect();
		this.stationClient.connect();

		if (shippingThread == null) {
			shippingThread = new ShippingThread();
			shippingThread.start();
		} else {
			print("busy to process previous request");
		}
	}

	// setters & getters
	public int getCurrentStation() {
		return currentStation;
	}

	public void setCurrentStation(int currentStation) {
		this.currentStation = currentStation;
	}

	public int getNextStation() {
		return nextStation;
	}

	public void setNextStation(int nextStation) {
		this.nextStation = nextStation;
	}

	public int getPrevStation() {
		return prevStation;
	}

	public void setPrevStation(int prevStation) {
		this.prevStation = prevStation;
	}

	@Override
	public void update(Observable o, Object arg) {

	}

	@Override
	public void print(String str) {
		if (debugEnabled == true) {
			System.out.println("[" + System.currentTimeMillis() / 1000 + "."
					+ System.currentTimeMillis() % 1000 + "]" + "["
					+ this.getClass().getName() + "] - " + str);
		}
	}

}
