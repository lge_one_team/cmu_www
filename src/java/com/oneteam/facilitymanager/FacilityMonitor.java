package com.oneteam.facilitymanager;

import java.util.Observable;
import java.util.Observer;

public class FacilityMonitor extends Observable implements Observer, DebugUtil {

	private static FacilityMonitor instance;

	private RobotStatus robotStatus;
	private StationStatus stationStatus;

	private RobotClient robotClient;
	private StationClient stationClient;

	private boolean enablePolling = false;

	private MonitorThread monitorThread = null;

	private static final int POLLING_PERIOD = 1000; // 1000 ms

	class MonitorThread extends Thread {
		public void run() {
			while (true) {
				
				if (enablePolling == true) {
					int retPos = stationClient.getRobotPositionEx();
					
					if ((retPos == 0) && (stationStatus.getCommStatus() == StationStatus.STATION_STATUS_DISCONNECTED))
					{
						stationClient.disconnect();
						try {
							sleep(POLLING_PERIOD);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						stationClient.connect();
					}
					
					stationStatus.setCurrentStation(retPos);
					
					if ((robotClient.getStatusEx() == false) && (robotStatus.getCommStatus() == RobotStatus.ROBOT_STATUS_DISCONNECTED))
					{
						robotClient.disconnect();
						try {
							sleep(POLLING_PERIOD);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						robotClient.connect();
					}
				}
				try {
					sleep(POLLING_PERIOD);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private boolean debugEnabled = true;

	private FacilityMonitor() {

		robotClient = RobotClient.getInstance();
		stationClient = StationClient.getInstance();
		robotStatus = RobotStatus.getInstance();
		stationStatus = StationStatus.getInstance();

		robotStatus.addObserver(this);
		stationStatus.addObserver(this);

		monitorThread = new MonitorThread();
		monitorThread.start();

	}

	protected static FacilityMonitor getInstance() {
		if (instance == null) {
			instance = new FacilityMonitor();
		}

		return instance;
	}

	public void enableMonitor() {
		enablePolling = true;
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		print("Update.....");
		if (arg0 instanceof RobotStatus) {
			print("RobotStatus Update");

		} else if (arg0 instanceof StationStatus) {
			print("StationStatus Update");
		}

		this.setChanged();
		this.notifyObservers(this);
	}

	@Override
	public void print(String str) {
		if (debugEnabled == true) {
			System.out.println("[" + this.getClass().getName() + "] - " + str);
		}
	}

}
