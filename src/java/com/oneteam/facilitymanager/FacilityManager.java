package com.oneteam.facilitymanager;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;

public class FacilityManager extends Observable implements Observer, DebugUtil {

	private static FacilityManager instance;
	private StationStatus stationStatus;
	private RobotStatus robotStatus;
	private ArrayList<OrderWidget> orderList;

	private FacilityMonitor facilityMonitor;
	private ShippingManager shippingManager;

	// debug flag
	private boolean debugEnabled = true;

	private boolean statusChanged = false;

	private FacilityManager() {
		stationStatus = StationStatus.getInstance();
		robotStatus = RobotStatus.getInstance();

		shippingManager = ShippingManager.getInstance();
		facilityMonitor = FacilityMonitor.getInstance();

		shippingManager.addObserver(this);
		facilityMonitor.addObserver(this);
	}

	public static FacilityManager getInstance() {
		if (instance == null) {
			instance = new FacilityManager();
		}
		return instance;
	}

	// Private APIs
	private void notifyToShipment() {
		this.setChanged();
		this.notifyObservers(this);
	}

	private synchronized void notifyToSupervisor() {
		statusChanged = true;
		this.setChanged();
		this.notifyObservers(this);
	}

	// Public APIs

	// APIs For Shipment
	public void sendAnOrder(ArrayList<OrderWidget> orderList2) {

		facilityMonitor.enableMonitor();
		
		if (orderList2 != null) {
			this.orderList = orderList2;
			shippingManager.ShipAnOrder(orderList2);
		}
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public boolean isOrderCompleted() {

		if (orderList == null) {
			return false;
		}

		for (Iterator<OrderWidget> iterator = this.orderList.iterator(); iterator
				.hasNext();) {

			OrderWidget order = (OrderWidget) iterator.next();

			if (order.isLoaded() == false) {
				return false;
			}
		}

		return true;
	}

	// APIs for Supervisor
	public StationStatus getStationStatus() {
		return stationStatus;
	}

	public RobotStatus getRobotStatus() {
		return robotStatus;
	}

	private String convertPositionString(int Pos) {

		String posString = new String();

		switch (Pos) {
		case StationStatus.STATION_STATUS_POSITION_INVENTORY_1:
			posString = "INVENTORY_STATION_1";
			break;
		case StationStatus.STATION_STATUS_POSITION_INVENTORY_2:
			posString = "INVENTORY_STATION_2";
			break;
		case StationStatus.STATION_STATUS_POSITION_INVENTORY_3:
			posString = "INVENTORY_STATION_3";
			break;
		case StationStatus.STATION_STATUS_POSITION_SHIPPING:
			posString = "INVENTORY_SHIPPING_DOCK";
			break;
		default:
			posString = "UNKNOWN";
			break;
		}

		return posString;
	}

	private String convertStatusString(int status) {
		
		String statusString = new String();

		// public static final int ROBOT_STATUS_ARRIVED = 0;
		// public static final int ROBOT_STATUS_LINE_LOST = 1;
		// public static final int ROBOT_STATUS_BUSY = 2;
		// public static final int ROBOT_STATUS_IDLE = 3;
		// public static final int ROBOT_STATUS_PROTOCOL_ERR = 9;
		switch (status) {
		case RobotStatus.ROBOT_STATUS_ARRIVED:
			statusString = "ARRIVED";
			break;
		case RobotStatus.ROBOT_STATUS_LINE_LOST:
			statusString = "LOST";
			break;
		case RobotStatus.ROBOT_STATUS_BUSY:
			statusString = "IN PROGESS";
			break;
		case RobotStatus.ROBOT_STATUS_IDLE:
			statusString = "IDLE";
			break;
		case RobotStatus.ROBOT_STATUS_PROTOCOL_ERR:
			statusString = "PROTOCOL MISMATCH";
			break;
		default:
			break;
		}
		
		return statusString;
	}

	public synchronized String getFacilityStatus() {
		if (statusChanged == true) {
			String facilityStatus = new String();

			facilityStatus = "-------------------------------------\n";
			facilityStatus += "Communication Status\n";
			facilityStatus += "-------------------------------------\n";
			facilityStatus += ("Robot #1 : "
					+ (robotStatus.getCommStatus() == RobotStatus.ROBOT_STATUS_ONLINE ? "ONLINE"
							: "OFFLINE") + "\n");
			facilityStatus += ("Station #1 : "
					+ (stationStatus.getCommStatus() == StationStatus.STATION_STATUS_ONLINE ? "ONLINE"
							: "OFFLINE") + "\n\n");

			facilityStatus += "-------------------------------------\n";
			facilityStatus += "RobotStatus\n";
			facilityStatus += "-------------------------------------\n";
			facilityStatus += "Current Position : ";
			facilityStatus += convertPositionString(stationStatus
					.getCurrentStation()) + "\n";
			facilityStatus += "Next Station : ";
			facilityStatus += convertPositionString(stationStatus
					.getNextStation()) + "\n";
			facilityStatus += "Previous Station : ";
			facilityStatus += convertPositionString(stationStatus
					.getPrevStation()) + "\n";

			facilityStatus += "Status : " + convertStatusString(robotStatus.getWorkStatus()) + "\n";

			statusChanged = false;
			
			return facilityStatus;
		} else {
			return null;
		}

	}

	@Override
	public void update(Observable o, Object arg) {
		
		print("update....." + o.getClass().getName());
		
		// From ShippingManager
		if (o instanceof ShippingManager) {
			this.notifyToShipment();
		}

		// From FacilityMonitor
		if (o instanceof FacilityMonitor) {
			print("NotifyToSupervisor...");
			this.notifyToSupervisor();
		}

	}

	@Override
	public void print(String str) {
		if (debugEnabled == true) {
			System.out.println("[" + this.getClass().getName() + "] - " + str);
		}
	}
}
