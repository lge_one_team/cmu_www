package com.oneteam.facilitymanager;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class Client implements DebugUtil {

	// private constants
	protected static final int CONNECT_TIMEOUT = 10000; // 10000 ms.

	// public constants
	public static final int CONNECTED = 0;
	public static final int DISCONNECTED = 1;

	private String serverIp;
	private int port;

	protected int connected;

	// debug flag
	private boolean debugEnabled = true;

	public int isConnected() {
		return connected;
	}

	// private void setConnected(int connected) {
	// this.connected = connected;
	// }

	private Socket clientSocket;

	private BufferedWriter bufferWrite;
	private BufferedReader bufferRead;

	@SuppressWarnings("unused")
	private Client() {
		// force user to use Client() below
	}

	protected Client(String ip, int port) {
		this.serverIp = ip;
		this.port = port;
		connected = DISCONNECTED;
	}

	protected boolean connect() {

		if (connected == CONNECTED) {
			return true;
		}

		try {
			clientSocket = new Socket(serverIp, port);
			clientSocket.setSoTimeout(CONNECT_TIMEOUT);
		//	clientSocket.getTcpNoDelay();
			
		} catch (UnknownHostException e) {
			connected = DISCONNECTED;
			return false;
		} catch (IOException e) {
			connected = DISCONNECTED;
			return false;
		}

		try {
			bufferWrite = new BufferedWriter(new OutputStreamWriter(
					clientSocket.getOutputStream()));
			bufferRead = new BufferedReader(new InputStreamReader(
					clientSocket.getInputStream()));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}

		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		connected = CONNECTED;
		return true;
	}

	protected void disconnect() {
		try {
			bufferRead.close();
			bufferWrite.close();
			clientSocket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		connected = DISCONNECTED;
	}

	protected boolean sendMsg(String msg) {

		if (connected == DISCONNECTED) {
			print("sendMsg() failed");
			return false;
		}

		try {
			bufferWrite.flush();
			bufferWrite.write(msg, 0, msg.length());
			bufferWrite.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}

		return true;
	}

	protected String receiveMsg() {

		if (connected == DISCONNECTED) {
			return null;
		}

		String retMsg = null;

		try {

			retMsg = bufferRead.readLine();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (retMsg != null) {
			retMsg = retMsg.trim();
		}

		return retMsg;
	}

	@Override
	public void print(String str) {
		if (debugEnabled == true) {
			System.out.println("[" + this.getClass().getName() + "] - " + str);
		}
	}
}
