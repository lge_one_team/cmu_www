package com.oneteam.facilitymanager;

//import java.io.IOException;
//import java.io.InputStreamReader;

public class StationClient extends Client implements DebugUtil {

	private final static String serverIp = "128.237.240.189"; // aduino - cmu
	private final static int serverPort = 500;
	
//	private final static String serverIp = "127.0.0.1";
//	private final static int serverPort = 501;


//	private static final char[] STATION_GET_ROBOT_POS = { 'S', 'S', 'L', '0', '\n' };
//	private static final char[] STATION_GET_WORKER_COMPLETE =  { 'S', 'S', 'B', '0', '\n' };
//	private static final int INDEX_POSITION = 3;
	
	private static final char[] STATION_GET_ROBOT_POS_EXT = { '@', 'S', 'S', 'L', '0', '\n' };
	private static final char[] STATION_GET_WORKER_COMPLETE_EXT =  { '@','S', 'S', 'B', '0', '\n' };
	private static final int INDEX_POSITION_EX = 4;
	
	private static StationClient instance;

	private static StationStatus status;
	
	// debug flag
	private boolean debugEnabled = true;


	private StationClient(String ip, int port) {
		super(ip, port);
	}

	public static StationClient getInstance() {
		if (instance == null) {
			instance = new StationClient(serverIp, serverPort);
			status = StationStatus.getInstance();
		}
		return instance;
	}

	protected synchronized boolean connect() {

		if (isConnected() == CONNECTED) {
			return true;
		}

		if (super.connect() == false) {
			print("connect() failed");
			return false;
		} else {
//			print("connect() succeeded");
			status.setCommStatus(StationStatus.STATION_STATUS_ONLINE);
			return true;
		}

	}

	protected synchronized void disconnect() {

		if (isConnected() == DISCONNECTED) {
			return;
		}

		super.disconnect();

		status.setCommStatus(StationStatus.STATION_STATUS_DISCONNECTED);
		
		return;
	}
	
	private int getPosition(char ch)
	{
		int pos = 0;
		
		pos = ch - '0';
		
		if ((pos > StationStatus.STATION_STATUS_POSITION_SHIPPING)  ||
				(pos < 0))
		{
			pos = 0;
		}
		
		return pos;
	}
	
	protected synchronized int getRobotPositionEx() {
		String retMsg = null;
		String msg = new String(STATION_GET_ROBOT_POS_EXT);
		int pos = 0;
		
		if (sendMsg(msg) == false)
		{
			status.setCommStatus(StationStatus.STATION_STATUS_PROTOCOL_ERR);
			return 0;
		}
		
		retMsg = receiveMsg();
		
		if (retMsg == null) {
			status.setCommStatus(StationStatus.STATION_STATUS_PROTOCOL_ERR);
			return 0;
		}
		
		if (!retMsg.regionMatches(0, msg, 0, INDEX_POSITION_EX)) {
			status.setCommStatus(StationStatus.STATION_STATUS_PROTOCOL_ERR);
			return 0;
		}
		
		pos = getPosition(retMsg.charAt(INDEX_POSITION_EX));
		
		status.setCurrentStation(pos);
		
		return pos;
	}

//	protected synchronized int getRobotPosition() {
//		
//		String retMsg = null;
//		String msg = new String(STATION_GET_ROBOT_POS);
//		
//		sendMsg(msg);
//		
//		retMsg = receiveMsg();
//		
//		if (retMsg.charAt(0) != 'S' || retMsg.charAt(1) != 'S' ||
//				retMsg.charAt(2) != 'L') {
//			// PROTOCOL_ERROR
//			return 0;
//		}
//		
//		return getPosition(retMsg.charAt(INDEX_POSITION));
//	}
	
	protected synchronized int confirmWorkerCompleteEx()  {
		String retMsg = null;
		String msg = new String(STATION_GET_WORKER_COMPLETE_EXT);
		
		int pos = 0;
		
		if (sendMsg(msg) == false)
		{
			status.setCommStatus(StationStatus.STATION_STATUS_PROTOCOL_ERR);
			return 0;
		}
		
		retMsg = receiveMsg();
		
		if (retMsg == null) {
			status.setCommStatus(StationStatus.STATION_STATUS_PROTOCOL_ERR);
			return 0;
		}
		
		if (!retMsg.regionMatches(0, msg, 0, INDEX_POSITION_EX)) {
			status.setCommStatus(StationStatus.STATION_STATUS_PROTOCOL_ERR);
			return 0;
		}
		
		pos = getPosition(retMsg.charAt(INDEX_POSITION_EX));
		
//		System.out.println("confirmWorkerCompleteEx : " + retMsg);
//		System.out.println("confirmWorkerCompleteEx (int) : " + pos);
		
		return pos;
		
	}

//	protected synchronized int confirmWorkerComplete() {
//		
//		String retMsg = null;
//		String msg = new String(STATION_GET_WORKER_COMPLETE);
//		
//		sendMsg(msg);
//		
//		retMsg = receiveMsg();
//		
//		if (retMsg.charAt(0) != 'S' || retMsg.charAt(1) != 'S' ||
//				retMsg.charAt(2) != 'B') {
//			// PROTOCOL_ERROR
//			return 0;
//		}
//		
//		return getPosition(retMsg.charAt(INDEX_POSITION));
//	}

	protected synchronized StationStatus getStatus() {
		return status;
	}
	
	@Override
	public void print(String str) {
		if (debugEnabled == true) {
			System.out.println("[" + this.getClass().getName() + "] - " + str);
		}
	}
	
	// TODO : Move to .....
	public static void main(String[] args) throws InterruptedException {
		
//		InputStreamReader insr = new InputStreamReader(System.in);

		StationClient stationClient = StationClient.getInstance();

		stationClient.connect();
		
		Thread.sleep(5000);

		while (true) {
			
			int pos = stationClient.getRobotPositionEx();
			
			System.out.println("Current Robot Pos : " + pos);
			
			Thread.sleep(1000);
			
//			try {
//				@SuppressWarnings("unused")
//				char ch = (char) insr.read();
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			} 
		}
	}

}
