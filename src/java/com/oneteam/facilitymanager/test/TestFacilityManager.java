package com.oneteam.facilitymanager.test;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import com.oneteam.facilitymanager.FacilityManager;
import com.oneteam.facilitymanager.OrderWidget;

public class TestFacilityManager implements Observer {

	public static final String WIDGET_NAME_BASKET_BALL = "basket ball";
	public static final String WIDGET_NAME_BASE_BALL = "base ball";
	public static final String WIDGET_NAME_SOCCER_BALL = "soccer ball";
	public static final String WIDGET_NAME_TENNIS_BALL = "tennis ball";
	public static final String WIDGET_NAME_GOLF_BALL = "golf ball";

	public static void main(String[] args) throws InterruptedException {

		TestFacilityManager testMgr = new TestFacilityManager();

		FacilityManager facilityManager = com.oneteam.facilitymanager.FacilityManager.getInstance();
		ArrayList<OrderWidget> orderList = new ArrayList<OrderWidget>();

		OrderWidget widget1 = new OrderWidget(1,
				WIDGET_NAME_BASE_BALL, 5);
		OrderWidget widget2 = new OrderWidget(1,
				WIDGET_NAME_BASKET_BALL, 5);
		OrderWidget widget3 = new OrderWidget(2,
				WIDGET_NAME_GOLF_BALL, 5);
		OrderWidget widget4 = new OrderWidget(2,
				WIDGET_NAME_SOCCER_BALL, 5);
		OrderWidget widget5 = new OrderWidget(3,
				WIDGET_NAME_TENNIS_BALL, 5);
		OrderWidget widget6 = new OrderWidget(3,
				WIDGET_NAME_BASE_BALL, 5);

		System.out.println("1. Make Order List");

		orderList.add(widget1);
		orderList.add(widget2);
		orderList.add(widget3);
		orderList.add(widget4);
		orderList.add(widget5);
		orderList.add(widget6);

		System.out.println("2. SendAnOrder");

		facilityManager.addObserver(testMgr);

		while (true) {
			
			System.out.println("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
			facilityManager.sendAnOrder(orderList);
			Thread.sleep(1000);
			
		}
	}

	@Override
	public void update(Observable arg0, Object arg1) {

		System.out.println("[TestFacilityManager] Update this.....");
		if (arg0 instanceof FacilityManager) {
			FacilityManager Mgr = FacilityManager.getInstance();

			System.out.println("Confirm Order Completion : "
					+ ((Mgr.isOrderCompleted() == true) ? "YES" : "NO"));
			
			String str = Mgr.getFacilityStatus();
			
			if (str != null)  {
				System.out.println(str);
			}

		}
	}
}
