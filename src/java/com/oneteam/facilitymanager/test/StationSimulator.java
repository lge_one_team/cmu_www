package com.oneteam.facilitymanager.test;

import java.io.IOException;

public class StationSimulator extends Simulator {

	private static int port = 501; // robot server port number

	public StationSimulator(int port) {
		super(port);
	}

	public static void main(String[] args) throws IOException {

		StationSimulator stationSimulator = new StationSimulator(port);

		stationSimulator.openSocket();

		byte buttonIndex = 0;
		byte sensorIndex = 0;

		while (true) {

			String recMsg = stationSimulator.receiveMsg();

			System.out.println("[StationSimulator] revMsg : " + recMsg);

			byte[] charStr = recMsg.getBytes();

			if (charStr[3] == 'B') {
				charStr[4] = (byte) ('1' + buttonIndex);
				buttonIndex++;
				buttonIndex %= 4;
			} else {
				charStr[4] = (byte) ('1' + sensorIndex);
				sensorIndex++;
				sensorIndex %= 4;
			}

			String sendMsg = new String(charStr);

			sendMsg += "\n";

			System.out.println("[StationSimulator] sendMsg :" + sendMsg);

			stationSimulator.sendMsg(sendMsg);

		}
		// robotSimulator.closeSocket();

	}
}
