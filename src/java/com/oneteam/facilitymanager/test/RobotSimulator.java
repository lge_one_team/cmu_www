package com.oneteam.facilitymanager.test;

import java.io.IOException;

public class RobotSimulator extends Simulator{
	
	private static int port = 500 ; // robot server port number

	public RobotSimulator(int port) {
		super(port);
	}

	public static void main(String[] args) throws IOException {
		
		RobotSimulator robotSimulator = new RobotSimulator(port);
		
		robotSimulator.openSocket();
		
		while(true) {
			
			String recMsg = robotSimulator.receiveMsg();
			
			System.out.println("[RobotSimulator] revMsg : "  + recMsg);
			
			byte[] charStr = recMsg.getBytes();
			charStr[2] = '1';
			
			String sendMsg = new String(charStr);
			
			sendMsg += "\n";
			
			robotSimulator.sendMsg(sendMsg);
			
		}
//		robotSimulator.closeSocket();
		
	}
}
