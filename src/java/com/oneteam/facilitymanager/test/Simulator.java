package com.oneteam.facilitymanager.test;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class Simulator {
	
	private ServerSocket serverSocket;
	private int port;
	private Socket clientSocket;
	
	BufferedWriter out = null;
	BufferedReader in = null;
	
	@SuppressWarnings("unused")
	private Simulator() {
		// TODO Auto-generated constructor stub
	}
	
	public Simulator(int port) {
		this.port = port;
	}
	
	public void openSocket()
	{
		try {
			serverSocket = new ServerSocket(this.port);
			System.out.println("\n\nWaiting for connection on port "
					+ port + ".");
		} catch (IOException e) {
			System.err.println("\n\nCould not instantiate socket on port: "
					+ port);
			System.exit(1);
		}
		
		try {
			clientSocket = serverSocket.accept();
		} catch (Exception e) {
			System.err.println("Accept failed.");
			System.exit(1);
		}
		
		System.out.println("Connection successful");
		System.out.println("Waiting for input.....");

		try {
			out = new BufferedWriter(new OutputStreamWriter(
					clientSocket.getOutputStream()));
			in = new BufferedReader(new InputStreamReader(
					clientSocket.getInputStream()));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void closeSocket() {
		
		try {
			out.close();
			in.close();
			clientSocket.close();
			serverSocket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public String receiveMsg() throws IOException {
		String recMsg = null;
		
		while ((recMsg = in.readLine()) != null) {
//			System.out.println("Read from Client");
//			System.out.println("Client Message: " + recMsg);
			break;
		} // while
		
		return recMsg;
	}
	
	public void sendMsg(String msg) throws IOException {
		
		out.write(msg, 0, msg.length());
		out.flush();

	}
}
