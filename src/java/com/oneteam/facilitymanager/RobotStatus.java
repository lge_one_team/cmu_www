package com.oneteam.facilitymanager;

import java.util.Observable;

public class RobotStatus extends Observable {

	// robot status
	public static final int ROBOT_STATUS_ARRIVED = 0;
	public static final int ROBOT_STATUS_LINE_LOST = 1;
	public static final int ROBOT_STATUS_BUSY = 2;
	public static final int ROBOT_STATUS_IDLE = 3;
	public static final int ROBOT_STATUS_PROTOCOL_ERR = 9;

	// robot comm. status
	public static final int ROBOT_STATUS_ONLINE = ROBOT_STATUS_PROTOCOL_ERR + 1;
	public static final int ROBOT_STATUS_DISCONNECTED = ROBOT_STATUS_ONLINE + 1;

	private static RobotStatus instance = null;

	// Properties
	private int commStatus;
	private int workStatus;

	private void notifyToMonitor() {
		this.setChanged();
		this.notifyObservers(this);
	}

	public int getWorkStatus() {
		return workStatus;
	}

	public void setWorkStatus(int workStatus) {
		if (this.workStatus != workStatus) {
			this.workStatus = workStatus;
			notifyToMonitor();
		}
	}

	public int getCommStatus() {
		return commStatus;
	}

	public void setCommStatus(int status) {
		if (this.commStatus != status) {
			this.commStatus = status;
			notifyToMonitor();
		}
	}

	private RobotStatus() {
		// do nothing
		commStatus = ROBOT_STATUS_DISCONNECTED;
		workStatus = ROBOT_STATUS_IDLE;
	}

	public static RobotStatus getInstance() {

		if (instance == null) {
			instance = new RobotStatus();
		}

		return instance;
	}

}
