package com.oneteam.facilitymanager;

import java.util.Observable;

public class StationStatus extends Observable implements DebugUtil {

	// station status
	public static final int STATION_STATUS_POSITION_UNKNOW = 0;
	public static final int STATION_STATUS_POSITION_INVENTORY_1 = 1;
	public static final int STATION_STATUS_POSITION_INVENTORY_2 = 2;
	public static final int STATION_STATUS_POSITION_INVENTORY_3 = 3;
	public static final int STATION_STATUS_POSITION_SHIPPING = 4;
	public static final int STATION_STATUS_PROTOCOL_ERR = 9;

	// communication status
	public static final int STATION_STATUS_ONLINE = STATION_STATUS_PROTOCOL_ERR + 1;
	public static final int STATION_STATUS_DISCONNECTED = STATION_STATUS_ONLINE + 1;

	private int commStatus;
	private int currentStation;
	private int nextStation;
	private int prevStation;

	private static boolean debugEnabled = true;

	public int getCommStatus() {
		return commStatus;
	}

	public void setCommStatus(int status) {
		if (commStatus != status) {
			commStatus = status;		
			notifyToMonitor();
		}
	}

	public int getCurrentStation() {
		return currentStation;
	}

	private void notifyToMonitor() {
		print("notifyToMonitor");
		this.setChanged();
		this.notifyObservers(this);
	}

	public void setCurrentStation(int position) {
		if (currentStation != position) {	

			if (position != STATION_STATUS_POSITION_UNKNOW) {

				prevStation = currentStation;
				currentStation = position;
				nextStation = ++position;
				
				nextStation %= (STATION_STATUS_POSITION_SHIPPING + 1);
				
				if (nextStation == 0)
				{
					nextStation = STATION_STATUS_POSITION_INVENTORY_1;
				}
			}
			else
			{
//				currentStation = position;
			}
			
			notifyToMonitor();
		}
	}

	public int getNextStation() {
		return nextStation;
	}

	private void setNextStation(int Station) {
		
		if (nextStation != STATION_STATUS_POSITION_UNKNOW) {
			nextStation = Station;
		}
	}

	public int getPrevStation() {
		return prevStation;
	}

	private void setPrevStation(int Station) {
		if (prevStation != STATION_STATUS_POSITION_UNKNOW) {
			prevStation = Station;
		}
	}

	private static StationStatus instance;

	private StationStatus() {
		// do nothing
		commStatus = STATION_STATUS_DISCONNECTED;
		currentStation = STATION_STATUS_POSITION_UNKNOW;
		nextStation = STATION_STATUS_POSITION_UNKNOW;
		prevStation = STATION_STATUS_POSITION_UNKNOW;
	}

	public static StationStatus getInstance() {
		if (instance == null) {
			instance = new StationStatus();
		}
		return instance;
	}

	@Override
	public void print(String str) {
		if (debugEnabled == true) {
			System.out.println("[" + this.getClass().getName() + "] - " + str);
		}

	}

}
