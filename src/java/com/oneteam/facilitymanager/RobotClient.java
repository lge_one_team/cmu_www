package com.oneteam.facilitymanager;

public class RobotClient extends Client implements DebugUtil {

	// private final static String serverIp = "10.255.99.218"; // gram
	// private final String serverIp = "128.237.250.207";

	 //robot - aduino
	 private final static String serverIp = "128.237.117.173";	 
	 private final static int portNum = 500;

	// simulator - gram
//	private final static String serverIp = "127.0.0.1";
//	private final static int portNum = 500;

//	private static final char[] ROBOT_CMD_GOTO_NEXT = { 'C', 'R', 'G', '0', '\n' };
//	private static final char[] ROBOT_GET_STATUS_ERROR = { 'S', 'R', 'E', '0', '\n' };
	
	private static final char[] ROBOT_CMD_GOTO_NEXT_EXT = { '@', 'C', 'R', 'G', '0', '\n'};
	private static final char[] ROBOT_GET_STATUS_EXT = { '@', 'S', 'R', 'E', '0', '\n'};

	private static RobotClient instance;

	private static RobotStatus robotStatus;

	private boolean debugEnabled = true;

	private RobotClient(String ip, int port) {
		super(ip, port);
	}

	protected static RobotClient getInstance() {

		if (instance == null) {
			instance = new RobotClient(serverIp, portNum);
			robotStatus = RobotStatus.getInstance();
		}
		return instance;
	}

	protected synchronized boolean connect() {

		if (isConnected() == CONNECTED) {
			return true;
		}

		if (super.connect() == false) {
			print("connect() failed");
			return false;
		} else {
			try {
				Thread.sleep(1000); // from experiment
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			robotStatus.setCommStatus(RobotStatus.ROBOT_STATUS_ONLINE);
			return true;
		}
	}

	protected synchronized void disconnect() {

		if (isConnected() == DISCONNECTED) {
			robotStatus.setCommStatus(RobotStatus.ROBOT_STATUS_DISCONNECTED);
			return;
		}

		super.disconnect();

		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		robotStatus.setCommStatus(RobotStatus.ROBOT_STATUS_DISCONNECTED);
		return;
	}

	private String receiveStatus() {
		String retMsg = null;

		retMsg = super.receiveMsg();

		if (retMsg == null) {
			// print("receiveMsg failed");
			return null;
		}

		// System.out.println("[RobotClient] receiveMsg succeeded");

		return retMsg;
	}

	protected synchronized boolean gotoNextStationEx() {
		
		print("gotoNextStationEx()");
		
		if (connected != CONNECTED) {
			robotStatus.setCommStatus(RobotStatus.ROBOT_STATUS_DISCONNECTED);
			return false;
		}
		
		// STEP 1. send command to robot
		String cmd = new String(ROBOT_CMD_GOTO_NEXT_EXT);
		sendMsg(cmd);

		// STEP 2. receive response from robot
		String retStr = receiveStatus();
		if (retStr == null) {
			print("ROBOT_CMD_GOTO_NEXT : ret null");
			robotStatus.setWorkStatus(RobotStatus.ROBOT_STATUS_PROTOCOL_ERR);
			return false;
		}
		
		if (!retStr.equals(retStr))
		{
			robotStatus.setWorkStatus(RobotStatus.ROBOT_STATUS_PROTOCOL_ERR);
			return false;
		}
		
//		robotStatus.setStatus(RobotStatus.ROBOT_OK);
		return true;
	}
	
//	protected synchronized boolean gotoNextStation() {
//
//		if (connected != CONNECTED) {
//			robotStatus.setStatus(RobotStatus.ROBOT_STATUS_DISCONNECTED);
//			return false;
//		}
//
//		String cmd = new String(ROBOT_CMD_GOTO_NEXT);
//
//		// STEP 1. send command to robot
//		sendMsg(cmd);
//
//		// STEP 2. receive response from robot
//		String retStr = receiveStatus();
//		if (retStr == null) {
//			print("ROBOT_CMD_GOTO_NEXT : ret null");
//			robotStatus.setStatus(RobotStatus.ROBOT_STATUS_PROTOCOL_ERR);
//			return false;
//		}
//		
//		if (retStr.charAt(0) != 'C' || retStr.charAt(1) != 'R'
//				|| retStr.charAt(2) != '1') {
//			robotStatus.setStatus(RobotStatus.ROBOT_STATUS_PROTOCOL_ERR);
//			return false;
//		}
//
////		robotStatus.setStatus(RobotStatus.ROBOT_OK);
//
//		return true;
//	}
	
	private boolean checkStatus(String str) {
		
		boolean ret = false;
		
		if (str == null) {
			robotStatus.setWorkStatus(RobotStatus.ROBOT_STATUS_PROTOCOL_ERR);
			return false;
		}
		
		if (str.isEmpty()) {
			robotStatus.setWorkStatus(RobotStatus.ROBOT_STATUS_PROTOCOL_ERR);
			return false;
		}
		
		if (!str.regionMatches(0, new String(ROBOT_GET_STATUS_EXT), 0, 4)) {
			robotStatus.setWorkStatus(RobotStatus.ROBOT_STATUS_PROTOCOL_ERR);
			return false;
		}
		
		/* check robot status which is located in 5th digit of ROBOT_GET_STATUS_EXT = { '@', 'S', 'R', 'E', '0', '\n'} */
		
		/*
			public static final int ROBOT_STATUS_ARRIVED = 0;
			public static final int ROBOT_STATUS_LINE_LOST = 1;
			public static final int ROBOT_STATUS_BUSY = 2;
			public static final int ROBOT_STATUS_DISCONNECTED = 3;
			public static final int ROBOT_STATUS_PROTOCOL_ERR = 9;
			public static final int ROBOT_STATUS_OTHER = -1;
		 */
		
		switch (str.charAt(4)) {
		case '0':
			robotStatus.setWorkStatus(RobotStatus.ROBOT_STATUS_ARRIVED);
			ret = true;
			break;
		case '1':
			robotStatus.setWorkStatus(RobotStatus.ROBOT_STATUS_LINE_LOST);
			ret = false;
			break;
		case '2':
			robotStatus.setWorkStatus(RobotStatus.ROBOT_STATUS_BUSY);
			ret = false;
			break;
//		case '3':
//			robotStatus.setStatus(RobotStatus.ROBOT_STATUS_DISCONNECTED);
//			ret = false;
//			break;
		case '9':
		default:
			robotStatus.setWorkStatus(RobotStatus.ROBOT_STATUS_PROTOCOL_ERR);
			ret = false;
			break;
		}
		
		return ret;
	}
	
	protected synchronized boolean getStatusEx() {
		
		if (connected != CONNECTED) {
			robotStatus.setCommStatus(RobotStatus.ROBOT_STATUS_DISCONNECTED);
			return false;
		}

		String cmd = new String(ROBOT_GET_STATUS_EXT);

		// STEP 1. send a comment to robot
		if (sendMsg(cmd) == false)
		{
			robotStatus.setCommStatus(RobotStatus.ROBOT_STATUS_DISCONNECTED);
			return false;
		}

		// STEP 2. receive the response from robot
		String retStr = receiveMsg();

		// STEP 3. check the protocol
		return checkStatus(retStr);
	
	}
	
//	protected synchronized boolean getStatus() {
//
//		if (connected != CONNECTED) {
//			robotStatus.setStatus(RobotStatus.ROBOT_STATUS_DISCONNECTED);
//			return false;
//		}
//
//		String cmd = new String(ROBOT_GET_STATUS_ERROR);
//
//		// STEP 1. send a comment to robot
//		sendMsg(cmd);
//
//		// STEP 2. receive the response from robot
//		String retStr = receiveMsg();
//
//		// System.out.println("Received Msg : " + retStr);
//
//		if (retStr == null) {
//			robotStatus.setStatus(RobotStatus.ROBOT_STATUS_PROTOCOL_ERR);
//			return false;
//		}
//
//		// STEP 3. check the protocol
//		if (retStr.charAt(0) != 'S' || retStr.charAt(1) != 'R') {
//			robotStatus.setStatus(RobotStatus.ROBOT_STATUS_PROTOCOL_ERR);
//			return false;
//		}
//
//		if (retStr.charAt(2) == 1) {
//
//		} else if (retStr.charAt(2) == 0) {
//
//		} else {
//			robotStatus.setStatus(RobotStatus.ROBOT_STATUS_PROTOCOL_ERR);
//			return false;
//		}
//
//		return true;
//	}

	@Override
	public void print(String str) {
		if (debugEnabled == true) {
			System.out.println("[" + System.currentTimeMillis() / 1000 +"]" + "[" + this.getClass().getName() + "] - " + str);
		}
	}

	// TODO : Move to .....
	public static void main(String[] args) throws InterruptedException {

		RobotClient robotClient = RobotClient.getInstance();

		System.out.println("[RobotClient] try to connect !!!");
		robotClient.connect();

		// Thread.sleep(10000);
		
		while (true) {
			
			System.out.println(System.currentTimeMillis() / 1000 + " : "+ "gotoNextStationEx");
			if (robotClient.gotoNextStationEx() == true) {

			} else {
				System.out.println("gotoNextStation failed !!!");
			}

			Thread.sleep(500);
			
			System.out.println(System.currentTimeMillis() / 1000 + " : "+ "getStatusEx");
			robotClient.getStatusEx();
		    Thread.sleep(500);
		}
	}

}
