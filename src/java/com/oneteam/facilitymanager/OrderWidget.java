package com.oneteam.facilitymanager;

//import java.util.ArrayList;

public class OrderWidget {


	public static final int MAX_INVENTORY_STATION = 3;

	private int stationId;
	private String widgetName;
	private int numOfWidget;
	private boolean isLoaded;

	public OrderWidget() {
		setStationId(0);
		setWidgetName(null);
		setNumOfWidget(0);
		setLoaded(false);
	}

	public OrderWidget(int stationId, String widgetName, int numOfWidget) {
		setStationId(stationId);
		setWidgetName(widgetName);
		setNumOfWidget(numOfWidget);
		setLoaded(false);
	}

	public int getStationId() {
		return stationId;
	}

	public void setStationId(int stationId) {
		this.stationId = stationId;
	}

	public String getWidgetName() {
		return widgetName;
	}

	public void setWidgetName(String widgetName) {
		this.widgetName = widgetName;
	}

	public int getNumOfWidget() {
		return numOfWidget;
	}

	public void setNumOfWidget(int numOfWidget) {
		this.numOfWidget = numOfWidget;
	}

	public boolean isLoaded() {
		return isLoaded;
	}

	public void setLoaded(boolean isLoaded) {
		this.isLoaded = isLoaded;
	}
}
