package com.oneteam.supervisorsystem;

public class InventoryStationData {
	int inventoryNum;
	String widgetName;
	int quantity;
	
	public InventoryStationData(int inventoryNum, String widgetName, int quantity) {
		this.inventoryNum = inventoryNum;
		this.widgetName = widgetName;
		this.quantity = quantity;
	}
	
	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	public int getInventoryNum() {
		return inventoryNum;
	}

	public String getWidgetName() {
		return widgetName;
	}
	public boolean searchItem(int inventoryNum, String widgetName){
		if(inventoryNum == this.getInventoryNum() && widgetName.equals(this.getWidgetName()))
			return true;
		return false;
	}

//	public void update(int quantity) {
//		setQuantity(quantity);
//		
//	}
//	
}
