package com.oneteam.supervisorsystem;

public class OrderData {
	String id;
	String status;
	String allItemsForOrder;
	public OrderData(String id, String status, String allItemsForOrder) {
		super();
		this.id = id;
		this.status = status;
		this.allItemsForOrder = allItemsForOrder;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getAllItemsForOrder() {
		return allItemsForOrder;
	}
	public void setAllItemsForOrder(String allItemsForOrder) {
		this.allItemsForOrder = allItemsForOrder;
	}
	

}
