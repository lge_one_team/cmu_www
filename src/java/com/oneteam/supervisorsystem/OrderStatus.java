package com.oneteam.supervisorsystem;

import java.util.*;

public class OrderStatus {
	ArrayList<OrderData> orderStatusArray;
	
	public OrderStatus() {
		orderStatusArray = new ArrayList<OrderData>();
	}
	
	public void setOrderStatus(OrderData od) {
		for(OrderData temp : orderStatusArray){
			if(od.getId().equals(temp.getId())){
				orderStatusArray.set(orderStatusArray.indexOf(temp), od);
				return;
			}
		}
		orderStatusArray.add(od);
	}
	
	public ArrayList<OrderData> getOrderStatus() {
		return orderStatusArray;
	}
}
