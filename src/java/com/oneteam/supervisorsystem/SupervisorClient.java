package com.oneteam.supervisorsystem;

import java.io.*;
import java.net.*;
import java.util.*;

public class SupervisorClient {
	
	/////////////////////////////////////////////////////////////////
	private static SupervisorClient instance;
	/////////////////////////////////////////////////////////////////

	/////////////////////////////////////////////////////////////////
    String ip = "127.0.0.1";
    int port = 2222;
    Socket s = null;
    ObjectInputStream ois;
    ObjectOutputStream oos;

    // Request
    private static final String MSG_ID_INVENTORY_DB_REQ 		= "SW1";
    private static final String MSG_ID_ADD_INVENTORY_REQ 		= "SW2";
    private static final String MSG_ID_FACILITY_STATUS_REQ 		= "SW3";
    private static final String MSG_ID_TOTAL_ORDER_STATUS_REQ 	= "SW4";
    private static final String MSG_ID_ORDER_STATUS_BYID_REQ 	= "SW5";
    // Response    
    private static final String MSG_ID_INVENTORY_DB_RES 		= "WS1";
    private static final String MSG_ID_ADD_INVENTORY_RES 		= "WS2";
    private static final String MSG_ID_FACILITY_STATUS_RES 		= "WS3";
    private static final String MSG_ID_TOTAL_ORDER_STATUS_RES 	= "WS4";      
    private static final String MSG_ID_ORDER_STATUS_BYID_RES 	= "WS5";      
    
    // Server originated message
    private static final String MSG_ID_INFO_CHANGED 	= "WS0";
  
    private static final String END 	= "E";
    private static final String SPLIT 	= ",";
    private static final String STATION_ITEM_CLASSIFICATION = "[ST]";
    
    private static final String DB_CHANGED = "DBChanged";
    private static final String FS_CHANGED = "FacilityStatusChanged";
    private static final String ORDER_CHANGED = "OrderChanged";
	/////////////////////////////////////////////////////////////////

	/////////////////////////////////////////////////////////////////    
	SupervisorGUI supervisorGUI;
	/////////////////////////////////////////////////////////////////

	/////////////////////////////////////////////////////////////////
	InventoryStatus inventoryStatus;
	OrderStatus orderStatus;
	FacilityStatus facilityStatus;
	/////////////////////////////////////////////////////////////////
    

	private SupervisorClient() {
		inventoryStatus = new InventoryStatus();
		orderStatus = new OrderStatus();
		facilityStatus = new FacilityStatus();
	}
	
	public static SupervisorClient getInstance() {
		if(instance == null) {
			instance = new SupervisorClient();
		}
		
		return instance;
	}
	
	public void startsupervisorsystem() {
		supervisorGUI = new SupervisorGUI();
		
		supervisorGUI.setVisible(true);

		System.out.println("[Supertvisor] GUI setVisible");    	

       	if(s == null) {
       		connect();
    	}
	
       	requestInventoryInfo();
       	requestOrderStatus();
       	requestFacilityStatus();
	}

    void requestInventoryInfo() {
        requestInfoForServer(MSG_ID_INVENTORY_DB_REQ);
    }

    void requestOrderStatus() {
        requestInfoForServer(MSG_ID_TOTAL_ORDER_STATUS_REQ);
    }

    void requestFacilityStatus() {
        requestInfoForServer(MSG_ID_FACILITY_STATUS_REQ);
    }
    
    void requestOrderStatusWithID(String id) {
    	StringBuilder msg = new StringBuilder();
    	msg.append(MSG_ID_ORDER_STATUS_BYID_REQ).append(SPLIT).append(id);
        requestInfoForServer(msg.toString());
    }
    
    void requestAddInventory(String inventoryNum, String widgetName, String widgetCount) {	
		StringBuilder msg = new StringBuilder();
       	if(s == null) {
       		connect();
    	}
        try {
            msg.append(MSG_ID_ADD_INVENTORY_REQ).append(SPLIT);
//            msg.append(STATION_ITEM_CLASSIFICATION).append(SPLIT);
            msg.append(inventoryNum).append(SPLIT);
            msg.append(widgetName).append(SPLIT);
            msg.append(widgetCount).append(SPLIT);
            msg.append(END);
            oos.writeObject(msg.toString());
            oos.flush();
        } catch (IOException ioe) {
        	ioe.printStackTrace();
        
            System.out.println("IOException");
        }
    }
    
	private void requestInfoForServer(String command) {
		StringBuilder msg = new StringBuilder();
       	if(s == null) {
       		connect();
    	}
        try {
            msg.append(command);
            msg.append(SPLIT);
            msg.append(END);
            oos.writeObject(msg.toString());
            oos.flush();
        } catch (IOException ioe) {
        	ioe.printStackTrace();
            System.out.println("IOException");
        }
	}

    void connect() {
        try {
 			s = new Socket(ip, port);
        	System.out.println("1.Socket");
            ois = new ObjectInputStream(s.getInputStream());
            oos = new ObjectOutputStream(s.getOutputStream());
            System.out.println("2.Stream");
            new SupervisorDAOThread().start();
            System.out.println("3.SupervisorDAOThread");
        } catch (Exception e) {
        	e.printStackTrace();
        	supervisorGUI.displayConnectionError();
            System.out.println("Connection Exception");
        }
    }	
    
    class SupervisorDAOThread extends Thread {

        public void run() {
            try {
                while (true) {
                    String msg = (String) ois.readObject();
                    System.out.println("Client : "+msg);
                    
                    StringTokenizer st = new StringTokenizer(msg, SPLIT);
                    String s = st.nextToken();
                    if(s.equals(MSG_ID_INVENTORY_DB_RES)) {
                    	log(MSG_ID_INVENTORY_DB_RES);
                        parseInventoryList(msg);
                        supervisorGUI.showInventoryList();
                        
                    }else if(s.equals(MSG_ID_ADD_INVENTORY_RES)) {
                    	supervisorGUI.showComplete();
                    	requestInventoryInfo();
                		log(MSG_ID_ADD_INVENTORY_RES);
                    }else if(s.equals(MSG_ID_FACILITY_STATUS_RES)) {
                    	facilityStatus.setFacilityStatus(st.nextToken());
                    	supervisorGUI.showFacilityStatus();
                    }else if(s.equals(MSG_ID_TOTAL_ORDER_STATUS_RES)) {                    	
                    	parseOrderStatus(msg);
                    	supervisorGUI.showOrderStatus();
                    }else if(s.equals(MSG_ID_ORDER_STATUS_BYID_RES)) {                    	
                    	parseSpecialOrderStatus(msg);
                    	supervisorGUI.showOrderStatus();
                    }else if(s.equals(MSG_ID_INFO_CHANGED)) {
                    	handleInfoChanged(msg);
                    }
                }
            } catch (Exception e) {
            	e.printStackTrace();
                System.out.println(e);
            }
        }

    }
    
    void parseInventoryList(String msg) {
    	String realMsg =  new String(msg.substring(MSG_ID_INVENTORY_DB_RES.length()+1, msg.length()-1));
    	System.out.println("parseInventoryList :" + realMsg);
        StringTokenizer st = new StringTokenizer(realMsg, "[");
        while(st.hasMoreTokens()) {
//        	System.out.println(st.nextToken());
        	String real_inventory_st = st.nextToken();
        	System.out.println("parseInventoryList real_inventory_st: "+ real_inventory_st);
        	int length_real_inventory_st =  real_inventory_st.length();
        	StringTokenizer inventory_st = new StringTokenizer(real_inventory_st.substring(4, length_real_inventory_st-1), SPLIT);
        	
        	// inventory station number
        	String stationId = inventory_st.nextToken();
        	int stationNumber = Integer.parseInt(stationId);
        	while(inventory_st.hasMoreTokens()) {
        		// widget item name
        		String name = inventory_st.nextToken();
		        // widget item count
        		String quantity = inventory_st.nextToken();
		        int widgetCount = Integer.parseInt(quantity);
	            		
	            inventoryStatus.addItem(stationNumber, name, widgetCount);
        	}
        }
	}

    void parseOrderStatus(String msg) {
        StringTokenizer st = new StringTokenizer(msg, SPLIT);
//        String temp = new String(st.nextToken());
        if(!st.nextToken().equals(MSG_ID_TOTAL_ORDER_STATUS_RES)) return;
        while(st.hasMoreTokens()){
        	String id = st.nextToken();
            if(id.equals(END) || id.equals("") || id.equals("none")) 
            	return;
        	String status = st.nextToken();
        	String widgetItems = st.nextToken();

    		orderStatus.setOrderStatus(new OrderData(id, status, widgetItems));
        }
	}
    void parseSpecialOrderStatus(String msg) {
        StringTokenizer st = new StringTokenizer(msg, SPLIT);
//        String temp = new String(st.nextToken());
        if(!st.nextToken().equals(MSG_ID_ORDER_STATUS_BYID_RES)) return;
        while(st.hasMoreTokens()){
        	String id = st.nextToken();
            if(id.equals(END) || id.equals("")) 
            	return;
        	String status = st.nextToken();
        	String widgetItems = st.nextToken();

    		orderStatus.setOrderStatus(new OrderData(id, status, widgetItems));
        }
	}
    
    public void handleInfoChanged(String msg) {
        StringTokenizer st = new StringTokenizer(msg, SPLIT);
        if(!st.nextToken().equals(MSG_ID_INFO_CHANGED)) return;
        String changeType = st.nextToken();
        
        if(changeType.equals(DB_CHANGED)) {
        	requestInventoryInfo();
        } else if(changeType.equals(FS_CHANGED)) {
        	requestFacilityStatus();
        } else if(changeType.equals(ORDER_CHANGED)) {
        	String orderId = st.nextToken();
        	requestOrderStatusWithID(orderId);        	
        }    	
    }
    
    public void addInventoryItem(String inventoryNum, String widgetName, String widgetNum) {
    	requestAddInventory(inventoryNum, widgetName, widgetNum);
    }
    
    public ArrayList<InventoryStationData> getInventory(int inventoryNum) {
    	return inventoryStatus.getItemListByInventoryID(inventoryNum);
    }
    
    public ArrayList<OrderData> getOrderStatus() {
    	return orderStatus.getOrderStatus();
    }
    
    public String getFacilityStatus() {
    	return facilityStatus.getFacilityStatus();
    }

    public ArrayList<String> getWidgetList() {
    	return inventoryStatus.getWidgetList();
    }    
    private void log(String command) {
        System.out.println("I got " + command + " message");
    	
    }
}
