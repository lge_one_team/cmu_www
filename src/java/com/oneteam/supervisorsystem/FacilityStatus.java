package com.oneteam.supervisorsystem;

public class FacilityStatus {
	String facilityStatusDescription;
	
	public FacilityStatus() {
		facilityStatusDescription = new String();
	}
	
	public void setFacilityStatus(String text) {
		facilityStatusDescription = text;
	}
	
	public String getFacilityStatus() {
		return facilityStatusDescription;
	}
}
