package com.oneteam.supervisorsystem;

import java.util.*;

public class InventoryStatus {
	ArrayList<InventoryStationData> allStationArray;
	
	public InventoryStatus() {
		allStationArray = new ArrayList<InventoryStationData>();
	}
	
	public void addItem(int inventoryNum, String name, int quantity) {
		for(InventoryStationData isd : allStationArray){
			if(isd.searchItem(inventoryNum, name)){
				isd.setQuantity(quantity);
				return;
			}
		}
		allStationArray.add(new InventoryStationData(inventoryNum, name, quantity));
		
//		System.out.println("InventoryStatus.addItem");
//		System.out.println(allStationArray.toString());
	}
	
	public ArrayList<InventoryStationData> getItemListByInventoryID(int inventoryNum) {
		ArrayList<InventoryStationData> inventoryWidgetList = new ArrayList<InventoryStationData>();
		
		for(InventoryStationData i : allStationArray) {
			if(i.inventoryNum == inventoryNum) {
				inventoryWidgetList.add(i);
			}
		}
		
		return inventoryWidgetList;
	}
	
	public ArrayList<String> getWidgetList() {
		ArrayList<String> wl = new ArrayList<String>();
		
		for(InventoryStationData i : allStationArray) {
			wl.add(i.getWidgetName());
		}
		HashSet hs = new HashSet(wl);
		ArrayList<String> newWidgetList = new ArrayList<String>(hs);

		return newWidgetList;
	}
}
